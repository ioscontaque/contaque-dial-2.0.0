import 'dart:async';
import 'dart:ui';
import 'package:dialer/services/mqtt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dialer/HomePage/CRMPage.dart';
import 'package:dialer/Login/Login.dart';
import 'dart:convert';
import 'dart:io';
import 'package:dialer/Url.dart';
import 'package:connectivity/connectivity.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:sqflite/sqflite.dart';
import 'package:dialer/Setting.dart';
import 'package:dialer/CallLogs/CallLogs.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:validators/validators.dart';
import '../database_helper.dart';
import '../sqlhelper.dart';
import 'ContactModel.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  HomePageState createState() => new HomePageState();
}

class HomePageState extends State<HomePage>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Animation slideAnimation;
  Animation demoSlideAnimation;
  AnimationController slideAnimController;
  AnimationController demoAnimController;
  static const platform = const MethodChannel('MyNativeChannel');
  String msg = "No Message";
  final txtNumer = new TextEditingController();
  String numbers = "";
  String common = "";
  bool loader = false;
  Url url = new Url();
  bool statusApi = true;
  String strUserFullName = "";
  bool isShowDialer = true; //show Dialer if is true
  List<UserContact> contacts = new List(); // contact list
  List<UserContact> showContacts = new List();
  UserContact removedContact;
  String inputSearchNumber = "";
  TextEditingController searchController = new TextEditingController();
  String displayTextChar = '!!';
  List<String> displayTextCharList = new List();
  FocusNode searchFocusNode = new FocusNode();
  SharedPreferences prefs;
  double dialerInputFontSize = 30;
  bool isCallInitiate = false;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  var refreshTime = 3;
  List<String> uniqueMobile = new List();
  DatabaseHelper helper;
  bool isShowLoader = false;
  bool isCallAlow = false;
  int campId;
  String campName;
  String skillName;
  bool isShowContacts = false;

  //String dialCode;
  String ipAddress;
  String accessToken;
  String selectedSkillName;
  String phoneNumber;
  bool isShowUIData = false;
  String userPhoneNo;
  bool isCheckCallStatus = true;
  double _height;
  bool isSlideAnimating = false;
  int activeBottomBarItem = 1;
  bool isSetCampDetails = false;
  bool isShowCallDemo = false;
  int selectedListId;
  int selectedSkillId;
  bool appbarVisiblity = true;
  String serverPhoneNo = "";
  String ring;
  String callStatus;
  bool showLogountloader = false;
  SqlProvider provider;
  List<Map<String, dynamic>> pauseCodesList = new List();
  bool isShowPauseCodeList = false;
  String userStatus = "";
  String agentId;
  Stream stream ;
  String hostName;

  init() async {
    WidgetsBinding.instance.addObserver(this);
    callStatus = await platform.invokeMethod('getCallStatus');
    prefs = await SharedPreferences.getInstance();
    agentId = prefs.getString("agentId");
    hostName = prefs.getString("hostName");
    ipAddress = prefs.getString("ipAddress") ?? "";
    accessToken = prefs.getString("accessToken") ?? "";

    serverPhoneNo = prefs.getString("ServerPhoneNumber") ?? "";
    ring = prefs.getString("ring") ?? "";
    ring = ring.trim();

    debugPrint("selectedRing $ring");
    provider = SqlProvider();
    await provider.open("pausedCode");
    pauseCodesList = await provider.db.query('pausecodeTable');
    debugPrint("QUERY Home ${pauseCodesList.length} , ${pauseCodesList}");
    debugPrint("ServerPhoneNo $serverPhoneNo , ring $ring");
    // openCRMPage();
    isSetCampDetails = prefs.getBool("setCampDetails") ?? false;
    setState(() {});
    await checkUserStatus();
    MqttService mqttService = MqttService(hostName: hostName);
    try{
      await mqttService.startService(agentId);
    }
    catch(e) {
      debugPrint("Exception:(mqtt) -> $e");
    }
    stream = mqttService.streamController.stream;
    stream.listen((event) {
      var eventJson = json.decode(event);
      String eventName = eventJson["data"]["event_name"];
      if(eventName =="CHANGE-STATUS"||eventName=="CRM-DISPOSE"||eventName=="CRM-HANGUP"){
        userStatusService();
      }
      else if(
      eventName=="FOLLOWUP-DIAL" || eventName == "PREVIEW-DIAL"||eventName=="CRM-DIAL"){
        prefs.setBool("openCrm", true);
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => CRMPage()));
      }
      else if(eventName =="MONITOR-LOGOUT") {
        prefs.setBool('loginStatus', false);
        prefs.remove("listid");
        prefs.remove("_allowCamp");
        prefs.remove("selectedData");
        Url.toastShow("Agent logout from monitor.", Colors.red);
        goToIntroPage();
      }
    });
  }

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 500), () {
      initAnimation();
      init();
    });
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    debugPrint("Application State :  $state");
    checkIOSCallState(state);
  }

  Future<void> checkIOSCallState(state) async {
    prefs = await SharedPreferences.getInstance();
    if (state == AppLifecycleState.inactive) {
      callStatus = await platform.invokeMethod('getCallStatus');
      if (this.mounted) {
        setState(() {});
      }
    }
    if (state == AppLifecycleState.resumed) {
      callStatus = await platform.invokeMethod('getCallStatus');
      // came back to Foreground
      if (isSetCampDetails == true) {
        openCRMPage();
      }
      userStatusService();
      setState(() {});
    }
  }

  Future<void> openCRMPage() async {
    try {
      var openCrm = prefs.getBool("openCrm") ?? false;
      debugPrint("OPNCRM $openCrm");
      if (openCrm == true || isCheckCallStatus == true) {
        isCheckCallStatus = false;
        debugPrint("OPeN OR ISCHKCLST");
        userStatusService();
      } else {
        setState(() {
          isShowUIData = true;
        });
      }
    } catch (e) {
      //Url.toastShow("Error : ${e.toString()}");
      debugPrint("Error(openCRMPage) -> ${e.toString()}");
    }
  }

  void initAnimation() async {
    prefs = await SharedPreferences.getInstance();
    // ********** Slide Animation **************
    slideAnimController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    slideAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(curve: Curves.ease, parent: slideAnimController));
    // ********** Slide Animation **************
    demoAnimController =
        AnimationController(vsync: this, duration: Duration(seconds: 3));
    demoSlideAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(curve: Curves.ease, parent: demoAnimController))
      ..addListener(() {
        setState(() {});
      });
    demoAnimController.repeat();
    isShowCallDemo = prefs.getBool("isShowCallDemo") ?? true;
  }

  Future<void> checkUserStatus() async {
    debugPrint("HOMEPAGE INIT");
    prefs = await SharedPreferences.getInstance();
    ipAddress = prefs.getString("ipAddress") ?? "";
    if (this.mounted) {
      setState(() {
        isShowUIData = false;
      });
    }
    isSetCampDetails = prefs.getBool("setCampDetails") ?? false;
    debugPrint("isSetCampDetails (HomePage) $isSetCampDetails");
    _checkPermissionStatus();
    getTran();
    isCheckCallStatus = true;
    getDataValue();
    isShowDialer = true;
    inputSearchNumber = "";
    openCRMPage();
  }

  // check userStatus by calling Api and Route page
  Future<void> userStatusService() async {
    var connectivityResult = await (new Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      HttpOverrides.global = new MyHttpOverrides();

      ipAddress = prefs.getString("ipAddress") ?? "";
      accessToken = prefs.getString("accessToken") ?? "";
      debugPrint(
          "USERSTATUSSERVICE URL ${ipAddress} , ${url.checkCallStatus}   , AccToken ${accessToken}");
      debugPrint("calling:: checkcallstatus");
      try {
        await http.post(ipAddress + url.checkCallStatus, headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "Authorization": accessToken
        }).then((response) async {
          String responseBody = response.body;
          var responseJSON = json.decode(responseBody);
          debugPrint("Response : $responseJSON");
          if (response.statusCode == 200) {
            if (responseJSON["response"] == "SUCCESS") {
              //prefs.setBool("openCrm", false);
              isCheckCallStatus = false; // for inbound call ..
              var arrData = responseJSON["data"];
              debugPrint("DDHOMe $arrData");
              var list = arrData;
              Map myMap = list;
              debugPrint("USerSTATUSSERVICE MAPDATA ${myMap}");
              userStatus = myMap["status"] ?? "";
              if (myMap["status"] == "AUX") {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => Setting()));
              } else if (arrData["status"] == "INCALL" ||
                  arrData["status"] == "ACW" ||
                  arrData["status"] == "DIAL" ||
                  arrData["status"] == "FOLLOWUP" ||
                  arrData["status"] == "QUEUE") {
                if (myMap["status"] == "ACW") {
                  userStatus = "Customer Down";
                }
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => CRMPage()),
                    (route) => false);
              } else {
                if (this.mounted) {
                  setState(() {
                    isShowUIData = true;
                  });
                }
              }
              if (this.mounted) {
                setState(() {});
              }
            } else if (responseJSON["response"] == "FAILURE" &&
                responseJSON["error"]["message"] == "User active from web") {
              Url.toastShow(responseJSON["error"]["message"]);
            }
          } else if (responseJSON["response"] == "FAILURE" &&
              (responseJSON["error"]["message"] == "Authorization Failure")) {
            prefs.setBool('loginStatus', false);
            prefs.remove("listid");
            prefs.remove("_allowCamp");
            prefs.remove("selectedData");
            Url.toastShow(responseJSON["error"]["message"], Colors.red);
            goToIntroPage();
          } else if (response.statusCode == 401) {
            prefs.setBool("loginStatus", false);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Login()), // Login
            );
          } else {
            Url.toastShow(responseJSON["message"], Colors.red);
          }
        }).timeout(Duration(seconds: 5));
      } on Exception catch (error) {
        // When Service is not Working
        debugPrint("Error : $error");
      }
    }
  }

  Future<void> updateUser() async {
    try {
      DatabaseHelper helper = new DatabaseHelper();
      var dbUser = await helper.getAllUser(); // get all user from db

      List<String> uniqueUserMobile = new List();

      if (dbUser != null && dbUser.length > 0) {
        contacts = [];

        // **for unique contacts list **
        String previousChar = '!!';
        for (var contact in dbUser) {
          //debugPrint("SHCDATA ${contact.fullName}");
          if (!uniqueUserMobile.contains(contact.mobileNo)) {
            if (previousChar.toLowerCase() !=
                    contact.fullName.trim()[0].toLowerCase() ??
                '#') {
              contacts.add(UserContact('||', '||'));
              previousChar = contact.fullName.trim()[0] ?? '#';
            }
            contacts.add(contact);
            uniqueUserMobile.add(contact.mobileNo);
          }
        }
        // ********
      }
      //debugPrint("SHOW CON: ${showContacts}");
      setState(() {
        showContacts = contacts;
        isShowContacts = true;
      });
    } catch (e) {}
  }
  getTran() async {
    var helper = new DatabaseHelper();
    Database database = await helper.db;
    try {
      await database.transaction((txn) async {
        txn.delete("User"); // perform delete operation
        await getContactList(txn);
      });
      updateUser();
    } catch (e) {
      if (database.isOpen) database.close(); // close db if get an exception
    }
  }

  // *** get contacts list from phone memory and store them in sqlite db ***
  Future<void> getContactList(var txn) async {
    try {
      // in build method in flutter lib... ->  iterable contact list ..
      Iterable<Contact> localContact =
          await ContactsService.getContacts(withThumbnails: false);
      String name;
      String number;
      var sortedContacts =
          localContact.where((item) => item.displayName != null).toList();
      sortedContacts.sort((a, b) =>
          a.displayName.toLowerCase().compareTo(b.displayName.toLowerCase()));

      debugPrint("sortedContacts ${sortedContacts}");

      for (Contact contact in sortedContacts) {
        debugPrint("sortedC Data: ${contact.displayName}");
        name = contact.displayName ?? " ";
        number = (contact.phones != null && contact.phones.length > 0
                ? contact.phones.toList()[0].value.replaceAll(" ", "")
                : "") ??
            "";
        name = name.trim();
        number = number.trim();
        if (number.isNotEmpty || number.length > 0) {
          if (name.isEmpty || name == "") {
            name = number;
          }
          UserContact userContact = new UserContact(name, number);
          userContact.saveContact(userContact, txn); // store in User Table
        }
      }
    } catch (e) {}
    // show this automatically for new install during get data from contacts and
    // store in db

    updateUser(); // update user from db
    if (showContacts.length > 0 || contacts.length > 0) {
      prefs.setBool("autoRefreshContact", false);
    }
    // *******************
  }

  @override
  void dispose() {
      slideAnimController.dispose();
      demoAnimController.dispose();
      txtNumer.dispose();
      WidgetsBinding.instance.removeObserver(this);

    super.dispose();
  }

  // ** check permissionStatus ****
  Future<void> _checkPermissionStatus() async {
    try {
      var contactsStatus = await PermissionHandler()
          .checkPermissionStatus(PermissionGroup.contacts);
      if (contactsStatus == PermissionStatus.granted) {
        prefs.setBool("isContactsGranted", true);
      }
    } catch (e) {}
  }

  // ** get required variables from sharedPrefrences **
  Future<void> getDataValue() async {
    debugPrint("GETDATAVALUE (HomePage)");
    try {
      prefs = await SharedPreferences.getInstance();
      isCallAlow =
          (prefs.getString("ManualAllow") == "Y" ? true : false) ?? false;
      strUserFullName = prefs.getString("Userfullname") ?? '';
      campId = prefs.getInt("campId") ?? 0;
      selectedListId = prefs.getInt("selectedListId") ?? 0;
      selectedSkillId = prefs.getInt("selectedSkillId") ?? 0;
      accessToken = prefs.getString("accessToken") ?? "";
      ipAddress = prefs.getString("ipAddress") ?? "";
      skillName = prefs.getString("skillName") ?? "";
      selectedSkillName = prefs.getString("selectedSkillName") ?? "";
    } catch (e) {}
  }

  // refresh page on pull down page .. and get contacts from phone memory
  Future<Null> refreshList() async {
    try {
      return getTran().then(() {
        setState(() => showContacts = contacts);
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    displayTextCharList = [];
    displayTextChar = '!!';
    _height = MediaQuery.of(context).size.height;

    return Stack(
      children: <Widget>[
        Scaffold(
            floatingActionButton: ring == null ||
                    ring == "Y" ||
                    serverPhoneNo.trim().isEmpty
                ? null //
                : IgnorePointer(
                    ignoring: callStatus == "CallConnect" ||
                            callStatus == "Call Connect"
                        ? true
                        : false,
                    child: Opacity(
                      opacity: callStatus == "CallConnect"
                          ? 0.5
                          : callStatus == "Call Connect"
                              ? 0.5
                              : 1,
                      child: IconButton(
                        iconSize: 55,
                        icon: Image.asset(
                          "assets/2.0x/join_icon.png",
                          fit: BoxFit.fill,
                          width: 100,
                          height: 100,
                        ),
                        onPressed: () async {
                          debugPrint("CallStatus $callStatus");
                          if (selectedSkillId == null || selectedSkillId == 0) {
                            Url.toastShow(
                              "please set settings details then you can use calling functionality.",
                            );
                            return;
                          }
                          if (callStatus != "CallConnect" ||
                              callStatus != "Call Connect") {
                            callStatus = "CallConnect";
                            setState(() {});
                            await initiateCall(serverPhoneNo);
                          }
                        },
                      ),
                    ),
                  ),
            bottomNavigationBar: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      border: Border(
                    top: BorderSide(color: Color(0xffa4a9aa)),
                  )),
                  height: 55,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        child: IconButton(
                          onPressed: () {
                            // _drawer();
                            debugPrint("Drawer");
                            _scaffoldKey.currentState.openDrawer();
                          },
                          icon: Image.asset(
                            'assets/2.0x/menu.png',
                            color: Color(0xff828580),
                            height: 25,
                            width: 25,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 8),
                        child: IconButton(
                          icon: isShowDialer == false
                              ? Image.asset(
                                  "assets/2.0x/dialpad.png",
                                  width: 30,
                                  height: 30,
                                  color: Colors.black,
                                )
                              : Image.asset(
                                  'assets/2.0x/contact-book.png',
                                  color: Color(0xff00476d),
                                  width: 45,
                                  height: 45,
                                ),
                          onPressed: () {
                            // Animation *********
                            if (slideAnimation.isCompleted) {
                              slideAnimController.reverse().orCancel;
                            } else {
                              slideAnimController.forward().orCancel;
                            }

                            // _searchByInput("");
                            searchController.text = "";
                            txtNumer.text = "";
                            common = "";
                            activeBottomBarItem = 1;
                            setState(() {
                              if (isShowDialer == true) {
                                isShowDialer =
                                    false; // change value of isShowDialer
                                //updateUser();
                                if (contacts.length > 0) {
                                  showContacts = contacts;
                                } else {
                                  updateUser();
                                }
                              } else {
                                SystemChannels.textInput
                                    .invokeMethod('TextInput.hide');
                                isShowDialer = true;
                              }
                            });
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        child: IconButton(
                          onPressed: () {
                            // _drawer();
                            prefs.setBool("showBackBtn", true);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  maintainState: false,
                                  builder: (context) => Setting()),
                            );
                          },
                          icon: Image.asset(
                            'assets/2.0x/settings.png',
                            color: Color(0xff828580),
                            height: 25,
                            width: 25,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Visibility(
                  visible: isShowCallDemo == true && isShowDialer == false,
                  child: Container(
                    height: 55,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(color: Colors.black45),
                    child: BackdropFilter(
                      filter: new ImageFilter.blur(sigmaX: 1.0, sigmaY: 1.0),
                    ),
                  ),
                )
              ],
            ),
            key: _scaffoldKey,
            backgroundColor: Colors.white,
            drawer: _drawer(),
            body: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                      top: 100,
                    ),
                    child: bodyWidget(),
                  ),
                  Container(
                    height: 100,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.bottomLeft,
                          end: Alignment.topRight,
                          stops: [0.4, 0.8, 1],
                          colors: [
                            // Colors are easy thanks to Flutter's Colors class.
                            Color(0xff00476d),
                            Color(0xff007981),
                            Color(0xff00a969),
                          ],
                        ),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(30),
                            bottomRight: Radius.circular(30))),
                    child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(bottom: 12),
                                  child: Text(
                                    "Contaque Dial",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontFamily: 'roboto-regular'),
                                  ),
                                ),
                              ],
                            ),
                            Visibility(
                              visible: true,
                              child: Row(
                                children: [
                                  Text(
                                    userStatus ?? "",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 14),
                                  )
                                ],
                              ),
                            )
                          ],
                        )),
                  ),
                  Visibility(
                    visible: !isShowDialer,
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 70,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Material(
                              elevation: 5.0,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(40)),
                              child: TextField(
                                // controller: TextEditingController(text: locations[0]),
                                controller: searchController,
                                focusNode: searchFocusNode,
                                onChanged: (value) {
                                  inputSearchNumber = value;
                                  _searchByInput(inputSearchNumber);
                                },
                                cursorColor: Theme.of(context).primaryColor,
                                decoration: InputDecoration(
                                    hintText: "Search",
                                    hintStyle: TextStyle(
                                        color: Color(0xffd2d3d1), fontSize: 16),
                                    prefixIcon: Material(
                                      elevation: 0.0,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(30)),
                                      child: Icon(
                                        Icons.search,
                                        color: Color(0xff828580),
                                      ),
                                    ),
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.symmetric(
                                        horizontal: 25, vertical: 13)),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                    visible: isCallInitiate,
                    child: Positioned(
                      top: MediaQuery.of(context).size.height / 2,
                      child: AlertDialog(
                        title: Text("Calling..."),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: isShowCallDemo == true && isShowDialer == false,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: double.infinity,
                      decoration: BoxDecoration(color: Colors.black45),
                      child: BackdropFilter(
                        filter: new ImageFilter.blur(sigmaX: 1.0, sigmaY: 1.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              height: 50,
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 10),
                              child: Align(
                                alignment: Alignment.topRight,
                                child: IconButton(
                                  icon: CircleAvatar(
                                    radius: 30,
                                    backgroundColor: Colors.white,
                                    child: Icon(
                                      Icons.clear,
                                      color: Colors.red,
                                      size: 30,
                                    ),
                                  ),
                                  onPressed: () {
                                    prefs.setBool("isShowCallDemo", false);
                                    isShowCallDemo = false;
                                    setState(() {});
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 50,
                            ),
                            Center(
                              child: Container(
                                child: Text(
                                  "Right swipe to initiate a call",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Stack(
                              children: <Widget>[
                                Container(
                                  color: Colors.green,
                                  height: 50,
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.only(left: 35),
                                          child: Image.asset(
                                            'assets/2.0x/call.png',
                                            color: Colors.white,
                                            height: 18,
                                            width: 18,
                                          )),
                                      Padding(
                                        padding: EdgeInsets.only(left: 20),
                                        child: Text(
                                          "Call",
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: Colors.white,
                                              fontFamily: 'roboto-regular'),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Transform(
                                  transform: Matrix4.translationValues(
                                      demoSlideAnimation != null
                                          ? (demoSlideAnimation.value *
                                              MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2)
                                          : 0.0,
                                      0,
                                      0),
                                  child: Container(
                                    height: 50,
                                    width: MediaQuery.of(context).size.width,
                                    color: Colors.white,
                                    padding: EdgeInsets.only(left: 40),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          child: Text(
                                            "Mike",
                                            style: TextStyle(
                                                fontFamily: 'roboto-regular',
                                                color: Color(0xff0f0f0f),
                                                fontSize: 16),
                                          ),
                                        ),
                                        Container(
                                          child: Text("xxxxxxxxxx",
                                              style: TextStyle(
                                                  fontFamily: 'roboto-regular',
                                                  color: Color(0xff5b5c59),
                                                  fontSize: 14)),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                            Transform(
                              transform: Matrix4.translationValues(
                                  demoSlideAnimation != null
                                      ? (demoSlideAnimation.value *
                                          MediaQuery.of(context).size.width /
                                          2)
                                      : 0.0,
                                  0,
                                  0),
                              child: Container(
                                margin: EdgeInsets.only(left: 20),
                                height: 50,
                                width: 50,
                                child: Image.asset("assets/2.0x/clicker.png"),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: loader,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 30,
                          height: 30,
                          margin: EdgeInsets.only(top: 200),
                          child: CircularProgressIndicator(),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )),
      ],
    );
  }

  Widget bodyWidget() {
    return showContacts != null
        ? Stack(
            children: <Widget>[
              showContacts.length != 0
                  ? Visibility(
                      visible: true,
                      child: Container(
                        margin: EdgeInsets.only(bottom: 0),
                        child: ListView.builder(
                            physics: BouncingScrollPhysics(),
                            itemCount:
                                showContacts != null && showContacts.length > 0
                                    ? showContacts.length
                                    : 0,
                            itemBuilder: (BuildContext context, int index) {
                              return customContactItem(index);
                            }),
                      ),
                    )
                  : Positioned(
                      top: 200,
                      child: Visibility(
                        visible: isShowContacts,
                        child: Center(
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "No Contacts Found.",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 18),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
              slideAnimController != null
                  ? AnimatedBuilder(
                      animation: slideAnimController,
                      builder: (BuildContext context, Widget child) {
                        return Transform(
                          transform: Matrix4.translationValues(
                              0.0,
                              _height *
                                  (slideAnimation != null
                                      ? slideAnimation.value
                                      : 1.0),
                              0.0),
                          child: child,
                        );
                      },
                      child: Container(
                        color: Colors.white,
                        padding: EdgeInsets.only(top: 0, bottom: 0),
                        height: MediaQuery.of(context).size.height - 20,
                        child: Container(
                          margin: EdgeInsets.only(top: 40),
                          child: Column(
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border(
//                            top: BorderSide(
//                              color: Color(0xffa4a9aa),
//                            ),
                                        bottom: BorderSide(
                                          color: Color(0xffa4a9aa),
                                        ),
                                        right: BorderSide(
                                          color: Color(0xffa4a9aa),
                                        ),
                                        left: BorderSide(
                                          color: Color(0xffa4a9aa),
                                        ))),
                                height: 60,
                                width: MediaQuery.of(context).size.width,
                                child: new Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: InkWell(
                                        onLongPress: () {
                                          FocusScope.of(context)
                                              .requestFocus(FocusNode());
                                        },
                                        child: new TextFormField(
                                          onTap: () {
                                            FocusScope.of(context)
                                                .requestFocus(FocusNode());
                                          },
                                          enableInteractiveSelection: true,
                                          enabled: true,
                                          controller: txtNumer,
                                          inputFormatters: [
                                            LengthLimitingTextInputFormatter(
                                                200),
                                          ],
                                          style: const TextStyle(
                                              color: Color(0xff62635f),
                                              fontFamily: "roboto-regular",
                                              fontSize: 30),
                                          textAlign: TextAlign.center,
                                          decoration: new InputDecoration(
                                            fillColor: Colors.white,
                                            border: InputBorder.none,
                                            hintStyle: const TextStyle(
                                                color: Color(0xffa4a9aa),
                                                fontFamily: "roboto-regular",
                                                fontSize: 30),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(
                                          top: 0.0, right: 15.0),
                                      height: 30,
                                      width: 30,
                                      decoration: new BoxDecoration(),
                                      child: new GestureDetector(
                                        onTap: () {
                                          if (txtNumer.text.isNotEmpty) {
                                            txtNumer.text = txtNumer.text
                                                .substring(0,
                                                    txtNumer.text.length - 1);
                                            setState(() {});
                                          }
                                        },
                                        onLongPress: () {
                                          if (txtNumer.text.isNotEmpty) {
                                            txtNumer.text = "";
                                            setState(() {});
                                          }
                                        },
                                        child: Image.asset(
                                          'assets/2.0x/backspace.png',
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    //your elements here
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 8,
                                child: Container(
                                  padding: EdgeInsets.only(
                                      top: 20, left: 10, right: 10),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 2,
                                        child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            mainAxisSize: MainAxisSize.max,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              _numberButtonItem("1"),
                                              _numberButtonItem("2"),
                                              _numberButtonItem("3"),
                                            ]),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Row(
                                            mainAxisSize: MainAxisSize.max,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              _numberButtonItem("4"),
                                              _numberButtonItem("5"),
                                              _numberButtonItem("6"),
                                            ]),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Row(
                                            mainAxisSize: MainAxisSize.max,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              _numberButtonItem("7"),
                                              _numberButtonItem("8"),
                                              _numberButtonItem("9"),
                                            ]),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Row(
                                            mainAxisSize: MainAxisSize.max,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              _numberButtonItem("*"),
                                              _numberButtonItem("0"),
                                              _numberButtonItem("#"),
                                            ]),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(
                                                    top: 10.0),
                                                height: 70,
                                                width: 70,
                                                decoration: new BoxDecoration(
                                                    color: Color(0xff399933),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            40)),
                                                child: ButtonTheme(
                                                  child: new FlatButton(
                                                    child: Image.asset(
                                                      'assets/2.0x/call.png',
                                                      color: Colors.white,
                                                      width: 30,
                                                      height: 30,
                                                    ),
                                                    onPressed: () {
                                                      debugPrint("AGENTID $agentId");
                                                      userPhoneNo = txtNumer
                                                          .text
                                                          .toString();
                                                      executeAfterBuild();
                                                    },
                                                  ),
                                                ),
                                              ),
                                            ]),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  : Container(),
            ],
          )
        : Center(
            child: CircularProgressIndicator(),
          );
  }

  Widget _drawer() {
    return new Drawer(
        child: Stack(
      children: <Widget>[
        Container(
            width: 600,
            child: SvgPicture.asset(
              'assets/2.0x/menu_bg.svg',
              fit: BoxFit.cover,
            )),
        Container(
          child: new ListView(
            padding: const EdgeInsets.all(0.0),
            children: <Widget>[
              new DrawerHeader(
                decoration: new BoxDecoration(
                    border: Border(bottom: BorderSide(color: Colors.white))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Image.asset(
                          'assets/2.0x/user.png',
                          width: 60,
                          height: 60,
                        )),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                              child: Text(strUserFullName,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: 20.0, color: Colors.white))),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                child: ListTile(
                  leading: Image.asset(
                    'assets/2.0x/call_log.png',
                    color: Colors.white,
                    width: 25,
                    height: 25,
                  ),
                  title: new Text(
                    'Call Logs',
                    style: TextStyle(
                        fontFamily: 'roboto-regular',
                        color: Colors.white,
                        fontSize: 18),
                  ),
                  onTap: () async {
                    var connectivityResult =
                        await (new Connectivity().checkConnectivity());
                    if (connectivityResult == ConnectivityResult.wifi ||
                        connectivityResult == ConnectivityResult.mobile) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => CallLogs()),
                      );
                    } else {
                      Url.toastShow("Kindly check your internet connection!!");
                    }
                  },
                ),
              ),
              Visibility(
                  visible: ring == null || ring == "N" ? false : true,
                  child: new Divider()),
              Visibility(
                visible: ring == null || ring == "N" ? false : true,
                child: new Container(
                  child: ListTile(
                    leading: Image.asset(
                      'assets/2.0x/status.png',
                      color: Colors.white,
                      width: 25,
                      height: 25,
                    ),
                    title: new Text(
                      'Change Status',
                      style: TextStyle(
                          fontFamily: 'roboto-regular',
                          color: Colors.white,
                          fontSize: 18),
                    ),
                    onTap: () {
                      isShowPauseCodeList = !isShowPauseCodeList;
                    },
                  ),
                ),
              ),
              AnimatedContainer(
                duration: const Duration(milliseconds: 200),
                height: isShowPauseCodeList == true
                    ? 45.0 * pauseCodesList.length
                    : 0.0,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: _createChildren(),
                  ),
                ),
              ),
              new Divider(), //            new Divider(),
              new Container(
                child: ListTile(
                  leading: Image.asset(
                    'assets/2.0x/logout.png',
                    color: Colors.white,
                    width: 25,
                    height: 25,
                  ),
                  title: new Text(
                    'Logout',
                    style: TextStyle(
                        fontFamily: 'roboto-regular',
                        fontSize: 18,
                        color: Colors.white),
                  ),
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return Center(
                          child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 30),
                            color: Colors.transparent,
                            width: 300.0,
                            height: 200.0,
                            child: Card(
                              margin: EdgeInsets.all(0),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(top: 20),
                                      child: Text(
                                        "LOGOUT",
                                        style: TextStyle(
                                            fontSize: 22,
                                            color: Color(0xff00476d),
                                            decoration: TextDecoration.none,
                                            fontFamily: 'roboto-regular'),
                                      ),
                                    ),
                                    SizedBox(height: 20),
                                    Container(
                                      child: Text(
                                        "Are you sure you want to exit?",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Color(0xff828580),
                                            decoration: TextDecoration.none,
                                            fontFamily: 'roboto-regular'),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Expanded(
                                      child: Container(
                                        height: 50,
                                        margin: EdgeInsets.only(top: 10),
                                        child: Container(
                                          decoration: BoxDecoration(
                                              border: Border(
                                                  top: BorderSide(
                                                      color:
                                                          Color(0xffa4a9aa)))),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Expanded(
                                                child: Container(
                                                  padding:
                                                      EdgeInsets.only(left: 4),
                                                  child: Material(
                                                      color: Colors.white,
                                                      child: InkWell(
                                                          onTap: () {
                                                            logOut();
                                                          },
                                                          child: Center(
                                                              child: Text("Yes",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          22,
                                                                      color: Color(
                                                                          0xff828580),
                                                                      decoration:
                                                                          TextDecoration
                                                                              .none,
                                                                      fontFamily:
                                                                          'roboto-regular'))))),
                                                ),
                                              ),
                                              Expanded(
                                                child: Container(
                                                  padding:
                                                      EdgeInsets.only(right: 4),
                                                  child: Material(
                                                      color: Colors.white,
                                                      child: InkWell(
                                                          onTap: () {
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                          child: Center(
                                                              child: Text(
                                                                  "Cancel",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          22,
                                                                      color: Color(
                                                                          0xff00476d),
                                                                      decoration:
                                                                          TextDecoration
                                                                              .none,
                                                                      fontFamily:
                                                                          'roboto-regular'))))),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  },
                ),
              ),
            ],
          ),
        )
      ],
    ));
  }

  List<Widget> _createChildren() {
    return new List<Widget>.generate(pauseCodesList.length, (int index) {
      return GestureDetector(
        onTap: ()async {
          var connectivityResult =
          await (new Connectivity().checkConnectivity());
          if (connectivityResult == ConnectivityResult.wifi ||
              connectivityResult == ConnectivityResult.mobile) {
            if (Navigator.canPop(context)) {
              isShowPauseCodeList = false;
              setUserStatus(pauseCodesList[index]["name"]);
            }
          } else {
            Url.toastShow("Kindly check your internet connection!!");
          }
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 40,
          padding: EdgeInsets.only(left: 90),
          child: Text(
            pauseCodesList[index]["details"].toString(),
            textAlign: TextAlign.start,
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
        ),
      );
    });
  }

  Widget _numberButtonItem(String numberText) {
    return Container(
        color: Colors.white,
        height: 50,
        width: 100,
        child: Material(
          color: Colors.white,
          child: InkWell(
            onTap: () {
              txtNumer.text += numberText;
            },
            child: Container(
              alignment: Alignment.center,
              child: Text(
                numberText,
                textAlign: TextAlign.start,
                style: TextStyle(
                    color: Color(0xff686966),
                    fontFamily: 'roboto-regular',
                    fontSize: 40,
                    fontWeight: FontWeight.w100),
              ),
            ),
          ),
        ));
  }

  Widget customContactItem(index) {
    if (showContacts[index].fullName == '||') {
      return Container(
        margin: EdgeInsets.only(
          left: 20,
          right: 12,
          top: 0,
        ),
        height: 30,
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              child: Text(
                (isAlpha(showContacts[index + 1].fullName.trim()[0])
                        ? showContacts[index + 1]
                            .fullName
                            .trim()[0]
                            .toUpperCase()
                        : '#') ??
                    '#',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xff828580),
                    fontFamily: 'roboto-regular',
                    fontSize: 14),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(left: 4),
                height: 1,
                color: Color(0xffcbd0d1),
              ),
            )
          ],
        ),
      );
    } else {
      return Stack(
        children: <Widget>[
          Container(
            color: Colors.green,
            height: 45,
            child: Row(
              children: <Widget>[
                Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.only(left: 35),
                    child: Image.asset(
                      'assets/2.0x/call.png',
                      color: Colors.white,
                      height: 18,
                      width: 18,
                    )),
                Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Text(
                    "Call",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontFamily: 'roboto-regular'),
                  ),
                )
              ],
            ),
          ),
          Dismissible(
            onDismissed: (Null) {
              userPhoneNo = showContacts[index].mobileNo.toString() ?? "";
              // isCallInitiate = true;
              removedContact = showContacts[index];
              showContacts.remove(removedContact);
              Timer(Duration(milliseconds: 500), () {
                executeAfterBuild();
              });
              setState(() {});
              updateUser();
            },
            direction: DismissDirection.startToEnd,
            key: ValueKey(showContacts[index].mobileNo),
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
              color: Colors.white,
              height: 45,
              child: Container(
                padding: EdgeInsets.only(left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 2),
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        showContacts[index].fullName ?? "",
                        style: TextStyle(
                            fontFamily: 'roboto-regular',
                            color: Color(0xff0f0f0f),
                            fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        showContacts[index].mobileNo ?? "",
                        style: TextStyle(
                            fontFamily: 'roboto-regular',
                            color: Color(0xff5b5c59),
                            fontSize: 14),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      );
    }
  }

  showManulDialFailDailog({msg = ""}) {
    showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) {
          return Opacity(
              opacity: 1,
              child: Center(
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 30),
                  width: MediaQuery.of(context).size.width,
                  height: 150,
                  child: Material(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    child: Container(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(
                                  height: 5,
                                ),
                                Container(
                                  height: 40,
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 20,
                                  ),
                                  child: Text(
                                    "",
                                    style: TextStyle(
                                        fontSize: 17,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 20,
                                  ),
                                  child: Text(
                                    msg,
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w900,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Center(
                          child: Container(
                            height: 60,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                border: Border(
                                    top: BorderSide(
                                        width: 1, color: Color(0xffdbdbdb)))),
                            child: Material(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30)),
                              child: InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => CRMPage()));
                                },
                                child: Center(
                                    child: Text("OK",
                                        style: TextStyle(
                                            color: Colors.blueAccent,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20))),
                              ),
                            ),
                          ),
                        )
                      ],
                    )),
                  ),
                ),
              ));
        });
  }

  Widget handleLeadingChar(String nameChar) {
    debugPrint("NAME: $nameChar");
    nameChar = nameChar[0];
    nameChar = nameChar != '' ? nameChar : '#';
    debugPrint("nameChar: $nameChar");
    if (!displayTextCharList.contains(nameChar)) {
      debugPrint("InsideCont $nameChar");
      if (nameChar == displayTextChar) {
        debugPrint("inside empty");
        return Container(
          height: 0,
          width: 0,
        );
      } else {
        displayTextChar = nameChar;
        displayTextCharList.add(displayTextChar);
        return Container(
          margin: EdgeInsets.only(top: 40),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                color: Colors.red,
                child: Text(nameChar),
              ),
              Divider(
                color: Colors.red,
                height: 2,
                thickness: 5,
              )
            ],
          ),
        );
      }
    } else {
      debugPrint("Inside Char");
      return Text('');
    }
  }

  Future<void> setUserStatus(status) async {
    try {
      debugPrint("CALL : SET-USER-STATUS");
      var body = jsonEncode({
        "type": "APP-REQUEST",
        "data": {"status": status}
      });
      showLogountloader = true;
      _onLoading();
      setState(() {});
      accessToken = prefs.getString("accessToken") ?? "";
      debugPrint("ACCESSTOKEN USERSTATUS $accessToken , body $body");
      //debugPrint("API ${ipAddress+ur}")
      await http.post(ipAddress + url.setUSerStatus, body: body, headers: {
        "Accept": "application/json",
        "content-type": "application/json",
        "Authorization": accessToken
      }).then((response) async {
        String responseBody = response.body;
        var responseJSON = json.decode(responseBody);
        String responseStatus = responseJSON["response"].toString();
        if (responseStatus.toLowerCase() == "success") {
          userStatus = status;
          Fluttertoast.showToast(
              msg: "Status updated successfully",
              textColor: Colors.white,
              backgroundColor: Colors.green);
          showLogountloader = false;
          _onLoading();
          setState(() {});
          if (Navigator.canPop(context)) {
            Navigator.pop(context);
          }
        } else if (responseStatus.toLowerCase() == "failure") {
          showLogountloader = false;
          _onLoading();
          setState(() {});
          Fluttertoast.showToast(
              msg: responseJSON["error"]["message"],
              textColor: Colors.white,
              backgroundColor: Colors.red);
        }
        debugPrint("RESPONSE (API : SET-USER-STATUS) $responseJSON");
      });
    } catch (e) {
      showLogountloader = false;
      setState(() {});
      debugPrint("Exception (API-> SET-USER-STATUS) : ${e.toString()}");
    }
  }

  // ** Logout API Service **
  Future<void> logOut() async {
    var connectivityResult = await (new Connectivity().checkConnectivity());
    showLogountloader = true;
    _onLoading();
    if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      HttpOverrides.global = new MyHttpOverrides();
      Url loginUrl = new Url();

      accessToken = prefs.getString("accessToken") ?? "";
      ipAddress = prefs.getString("ipAddress") ?? "";

      debugPrint(
          "LOGOUT DATA: url ->  ${ipAddress} , ${loginUrl.logOut} ,accessToken ${accessToken} , ");

      try {
        await http.post(ipAddress + loginUrl.logOut, headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "Authorization": accessToken
        }).then((response) async {
          String responseBody = response.body;
          var responseJSON = json.decode(responseBody);
          debugPrint("Logout REs ${responseBody}");
          if (response.statusCode == 200) {
            showLogountloader = false;
            _onLoading();
            if (responseJSON["response"] == "SUCCESS") {
              prefs.setBool('loginStatus', false);
              // prefs.remove("dialCode");
              prefs.remove("listid");
              prefs.remove("_allowCamp");
              prefs.remove("selectedData");
              Url.toastShow("User Logout successfully", Colors.green);
              goToIntroPage();
            } else if (responseJSON["response"] == "FAILURE" &&
                responseJSON["error"]["message"] == "User active from web") {
              Url.toastShow(responseJSON["error"]["message"]);
            } else {
              showLogountloader = false;
              _onLoading();
              Fluttertoast.showToast(
                  msg: "User Logout unsuccessfully",
                  textColor: Colors.white,
                  backgroundColor: Colors.red);
            }
          } else if (responseJSON["response"] == "FAILURE" &&
              (responseJSON["error"]["message"] == "Authorization Failure")) {
            prefs.setBool('loginStatus', false);
            //  prefs.remove("dialCode");
            prefs.remove("listid");
            prefs.remove("_allowCamp");
            prefs.remove("selectedData");
            Url.toastShow(responseJSON["error"]["message"], Colors.red);
            goToIntroPage();
          } else if (responseJSON["response"] == "FAILURE" &&
              responseJSON["error"]["message"] == "User active from web") {
            Url.toastShow(responseJSON["error"]["message"]);
          } else {
            showLogountloader = false;
            _onLoading();
            Fluttertoast.showToast(
                msg: responseJSON["error"]["message"],
                textColor: Colors.white,
                backgroundColor: Colors.red);
          }
        }).timeout(const Duration(seconds: 20));
      } catch (e) {
        showLogountloader = false;
        _onLoading();
        Url.toastShow(
            "Something went wrong : $e ${loginUrl.login}", Colors.orange);
      }
    } else {
      showLogountloader = false;
      _onLoading();
      Url.toastShow("Kindly check your internet connection!!");
    }
  }

  Future<void> dialCallService() async {
    String listId;
    debugPrint("inside[] -> dialCallService , user_no ->  $userPhoneNo");
    userPhoneNo = userPhoneNo.trim();
    userPhoneNo = userPhoneNo.replaceAll("-", "");
    if (userPhoneNo == null || userPhoneNo.isEmpty || userPhoneNo == "") {
    } else {
      phoneNumber = prefs.getString("ServerPhoneNumber") ?? '';
      debugPrint(
          "dialCall [] ->  userno $userPhoneNo ,campId $campId , skill_name $skillName");
      debugPrint("server phone no $phoneNumber");
      var connectivityResult = await (new Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.wifi ||
          connectivityResult == ConnectivityResult.mobile) {
        HttpOverrides.global = new MyHttpOverrides();
        var body = jsonEncode({
          "type": "APP-REQUEST",
          "data": {
            "camp_id": campId,
            "skill_id": selectedSkillId,
            "phone_number": userPhoneNo,
            "list_id": selectedListId
          }
        });

        accessToken = prefs.getString("accessToken") ?? "";

        debugPrint(
            "CALL-API (MANUAL-DIAL) : body - ${body} , ip- ${ipAddress + url.dialCall} ,accessToken $accessToken ");
        //return;
        try {
          await http.post(ipAddress + url.dialCall, body: body, headers: {
            "Accept": "application/json",
            "content-type": "application/json",
            "Authorization": accessToken
          }).then((response) async {
            debugPrint("ManualApiResponse ${response.body}");
            var responseJSON = json.decode(response.body);
            if (response.statusCode == 200) {
              String responseBody = response.body;
//              var responseJSON = json.decode(responseBody);
              debugPrint("dialCallService() res-> ${responseJSON}");
              if (responseJSON["response"] == "SUCCESS") {
                String failType = responseJSON["data"]["fail_type"] ?? "";
                if (failType != "") {
                  if (failType == "FAIL" || failType == "TRUNKFAIL") {
                    //  showManulDialFailDailog(msg : "this number is in follow up.");
                    prefs.setBool("openCrm", true);
                    prefs.setString("dialNumber", userPhoneNo);
                    prefs.setString("CRM", "CRM");
                    prefs.setBool("is_init_call", true);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => CRMPage()));
                    debugPrint("DialCallSER isCAllI $isCallInitiate");
                  } else {
                    Fluttertoast.showToast(
                        msg: "getting some error",
                        backgroundColor: Colors.red,
                        textColor: Colors.white);
                  }
                } else {
                  prefs.setBool("openCrm", true);
                  prefs.setString("dialNumber", userPhoneNo);
                  prefs.setString("CRM", "CRM");
                  prefs.setBool("is_init_call", true);
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => CRMPage()));
                  debugPrint("DialCallSER isCAllI $isCallInitiate");
                }
                isCallInitiate = false;
                setState(() {});
              } else if (responseJSON["response"] == "FAILURE" &&
                  responseJSON["error"]["message"] == "User active from web") {
                Url.toastShow(responseJSON["error"]["message"]);
              } else {
                isCallInitiate = false;
                setState(() {});
              }
            } else if (responseJSON["response"] == "FAILURE" &&
                (responseJSON["error"]["message"] == "Authorization Failure")) {
              Fluttertoast.showToast(
                  msg: responseJSON["error"]["message"],
                  backgroundColor: Colors.red,
                  textColor: Colors.white);
            } else if (responseJSON["response"] == "FAILURE") {
              Url.toastShow(responseJSON["error"]["message"] ?? "");
              isCallInitiate = false;
              setState(() {});
            } else {
              isCallInitiate = false;
              String errorMsg = responseJSON["error"]["message"] ?? "";
              if (errorMsg != null || errorMsg != "") {
                Url.toastShow(errorMsg);
              } else {
                Url.toastShow("please try again.");
              }
              setState(() {});
            }
          }).timeout(const Duration(seconds: 10));
        } on Exception catch (error) {
          isCallInitiate = false;
          setState(() {});
          loader = false;
        }
      } else {
        Url.toastShow("Kindly check your internet connection!!");
      }
    }
  }

  void _onLoading() {
    if (showLogountloader == true) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => new Center(
          child: CircularProgressIndicator(
            valueColor:
                new AlwaysStoppedAnimation<Color>(new Color(0xFF4B8BF4)),
          ),
        ),
      );
    } else {
      if (Navigator.canPop(context)) {
        Navigator.pop(context);
      }
    }
  }

  Future<void> executeAfterBuild() async {
    debugPrint("executeAfterBuild");
    userPhoneNo = userPhoneNo.trim();
    if (userPhoneNo.isEmpty) {
      Fluttertoast.showToast(msg: "mobile number can't be blank");
      return;
    }
    if (selectedSkillId == null || selectedSkillId == 0) {
      Url.toastShow(
        "please set setting details then you can use calling functionality.",
      );
      return;
    }
    try {
      callStatus = await platform.invokeMethod('getCallStatus');
    } on PlatformException catch (e) {
      callStatus = "Failed to get data from native : '${e.message}'.";
    }
    msg = callStatus;
    debugPrint("msg1 $msg");
    if ((msg == "" || msg == "free") && ring == "N") {
      Url.toastShow("Please first join then you can call.");
      return;
    }
    setState(() {});
    if (msg == "" ||
        msg == "free" ||
        msg == "CallConnect" ||
        msg == "Call Connect" ||
        msg == "Call outGoing") {
      webServiceForstatus();
      return;
    }

    isCallInitiate = false;
    setState(() {});
  }

  Future<void> webServiceForstatus() async {
    var connectivityResult = await (new Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      HttpOverrides.global = new MyHttpOverrides();
      debugPrint("WEBSV4STTS URL ${ipAddress + url.checkCallStatus}");
      debugPrint("calling:: checkcallstatus");
      debugPrint("ACCTKN $accessToken");
      try {
        await http.post(ipAddress + url.checkCallStatus, headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "Authorization": accessToken
        }).then((response) async {
          var responseJSON = json.decode(response.body);
          if (response.statusCode == 200) {
            debugPrint("WEBSV4STTS $responseJSON");
            if (responseJSON["response"] == "SUCCESS") {
              var arrData = responseJSON["data"];
              if (arrData["status"] == "READY") {
                prefs.setString("dialNumber", userPhoneNo);
                debugPrint("webS4STatus -> $isCallInitiate");
                dialCallService();
              } else if (arrData["status"] == "PAUSED") {
                Url.toastShow("User in paused mode");
                isCallInitiate = false;
                setState(() {});
                prefs.setBool("showLoader", false);
              }
              // queue,
              if (arrData["status"] == "INCALL" ||
                  arrData["status"] == "ACW" ||
                  arrData["status"] == "DIAL" ||
                  arrData["status"] == "FOLLOWUP" ||
                  arrData["status"] == "QUEUE") {
                isCallInitiate = false;
                setState(() {});
                prefs.setBool("showLoader", false);
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => CRMPage()),
                    (route) => false);
              }
            } else if (responseJSON["response"] == "FAILURE" &&
                responseJSON["error"]["message"] == "User active from web") {
              Url.toastShow(responseJSON["error"]["message"]);
            } else {
              isCallInitiate = false;
              Url.toastShow(
                  "Something went wrong. try again later. \n Error Code : ${responseJSON["message"]}");
            }
          } else if (responseJSON["response"] == "FAILURE" &&
              (responseJSON["error"]["message"] == "Authorization Failure")) {
            prefs.setBool('loginStatus', false);
            // prefs.remove("dialCode");
            prefs.remove("listid");
            prefs.remove("_allowCamp");
            prefs.remove("selectedData");
            Url.toastShow(responseJSON["error"]["message"], Colors.red);
            goToIntroPage();
          } else if (response.statusCode == 401) {
            isCallInitiate = false;
            setState(() {});
            Url.toastShow(
                "Something went wrong. try again later. \n Error Code : ${response.statusCode.toString()}");
          } else {
            var responseJSON = json.decode(response.body);
            Url.toastShow(
                "Something went wrong. try again later. \n Error Code : ${responseJSON["message"]}");
          }
        }).timeout(Duration(seconds: 5));
      } on Exception catch (error) {
        isCallInitiate = false;
        setState(() {});
        debugPrint("Error : $error");
      }
    } else {
      Url.toastShow("Kindly check your internet connection!!");
    }
  }

  // ** this method take input {name or number} and return list of matching user
  void _searchByInput(String inputSearch) {
    List<UserContact> localList = new List(); // create localList
    try {
      if (inputSearch != null) {
        // input must be take an args
        if (isNumeric(inputSearch)) {
          // Enter input is number then
          for (UserContact contact in contacts) {
            if (contact.mobileNo.contains(inputSearch)) {
              // check mobile no match by input
              localList.add(contact);
            }
          }
        } else {
          // Enter input is string
          for (UserContact contact in contacts) {
            if (contact.fullName
                .toLowerCase()
                .trim()
                .contains(inputSearch.toLowerCase().trim())) {
              // check input match by name
              localList.add(contact);
            }
          }
        }
        setState(() {
          showContacts =
              localList; // update showContacts and change state of widget {by setState}
        });
      }
    } catch (e) {}
  }

  // ** Invoke Method getData in  {Android or IOS} (Native code -> platform dependent)
  Future<void> initiateCall(String number) async {
    prefs.setBool("openCrm", true);
    String message;
    try {
      Future.delayed(Duration(milliseconds: 100)).then((val) async {
        await platform.invokeMethod('getData', {"text": number});
      });

      setState(() {});
    } on PlatformException catch (e) {
      isCallInitiate = false;
      setState(() {});
      message = "Failed to get data from native : '${e.message}'.";
      //  Url.toastShow(message);
      debugPrint("Error (during call) -> ${message}");
    }
  }

  void goToIntroPage() {
    prefs.setBool("setCampDetails", false);
    prefs.setBool("loginStatus", false);
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => Login()),
        (Route<dynamic> route) => false);
  }

  showPauseCodeDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog();
        });
  }
}
