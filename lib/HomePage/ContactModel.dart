class UserContact {
  int id ;
  String fullName ;
  String mobileNo ;
  UserContact(String name , String number) {
    this.fullName = name ;
    this.mobileNo = number ;
  }
  setUserId(int id) {
    this.id = id ;
  }

  // convert object to Map
  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["fullname"] = fullName;
    map["mobile_no"] = mobileNo;
    return map;
  }


  // contacts store in (User) table in db using transaction
  void saveContact(UserContact contact , var txn)  {
    txn.insert("User", contact.toMap());
  }

}