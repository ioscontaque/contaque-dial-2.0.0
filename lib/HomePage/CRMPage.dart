import 'dart:async';
import 'dart:math';

import 'package:dialer/Model/crmItemModel.dart';
import 'package:dialer/custom_appbar.dart';
import 'package:dialer/custom_centered_widget.dart';
import 'package:dialer/services/mqtt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:dialer/HomePage/HomePage.dart';
import 'package:dialer/Login/Login.dart';
import 'dart:convert';
import 'dart:io';
import 'package:dialer/Url.dart';
import 'package:connectivity/connectivity.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:dialer/HomePage/CRMColumnModel.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:fluttercontactpicker/fluttercontactpicker.dart';

import '../Setting.dart';

class CRMPage extends StatefulWidget {
  const CRMPage({Key key}) : super(key: key);

  @override
  CRMPageState createState() => new CRMPageState();
}

class CRMPageState extends State<CRMPage>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  static const platform = const MethodChannel('MyNativeChannel');
  String msg = "No Message";
  SharedPreferences prefs;

  final txtAddress = new TextEditingController();
  final txtFirstName = new TextEditingController();
  final txtPhoneNumber = new TextEditingController();
  final txtLastName = new TextEditingController();
  String numbers = "";
  String common = "";
  bool loader = false;
  String fullName = "";
  Url url = new Url();
  List dispositionData = [];
  Map strDisposeList;
  String disposeName;
  List isSelectedDesposeValue = [];
  List disposeData = [];
  String externalURL = "";
  String dispositionType;
  String dispositionName;
  bool visibleData = false;
  bool selectDate = false;
  bool getfunc = false;
  final txtConferencetime = new TextEditingController();
  bool showLoader = false;
  bool genderVal;

  String groupValue = "gender";
  List<String> crmControllerValue = new List<String>();
  String normalDropdownValue;
  bool isShowCustomDispoDate = true;
  String ShowCustomDispoDate = "";

  bool isSelectFollowUpType = false;


  final format = DateFormat("yyyy-MM-dd HH:mm");
  List<TextEditingController> _controllers = new List();
  List<CRMColumnModel> arrColumn = new List<CRMColumnModel>();
  bool editable = false;
  DateTime date;
  bool _isWebViewVisible = true; // for WebViewScaffold
  bool isShowUI = false;
  bool isShowRefresh = true;
  bool isShowProgressBar = false;
  List<CRMItemModel> crmItemModelList = new List<CRMItemModel>();
  Map<String, dynamic> crmValuesList = new Map<String, dynamic> ();
  int skipIndex = -1;
  List<String> dispositionNameData = new List();
  Map<String, String> genderMap = new Map();
  List<String> numbersList = new List<String>();
  String selectedNumber;

  String accessToken;

  String ipAddress;
  String userStatus;
  bool isCallAlow = false;
  String strUserFullName;
  int campId;
  int selectedSkillId;
  int selectedListId;
  String skillName;
  String selectedSkillName;
  String userPhoneNo;
  bool isCallInitiate;
  String phoneNumber;
  TextEditingController txtNubmber = TextEditingController();
  Animation slideAnimation;
  AnimationController slideAnimController;
  bool isSlideAnimating = false;
  bool isShowDialPad = false;
  Random random = Random();
  bool isShowDialog = false;
  Stream stream ;
  String hostName;
  String agentId;


  init() async {

    userStatusService();
    getDataValue();
    prefs = await SharedPreferences.getInstance();
    agentId = prefs.getString("agentId");
    hostName = prefs.getString("hostName");
    MqttService mqttService = MqttService(hostName: hostName);
    await mqttService.startService(agentId);
    // await mqtt(agentId,streamController);
    stream = mqttService.streamController.stream;
    stream.listen((event) {
      var eventJson = json.decode(event);
      String eventName = eventJson["data"]["event_name"];
      debugPrint("EventName $eventName");
      if(eventName == "ACW" || eventName=="QUEUE"||eventName == "MANUAL-DIAL"||
       eventName=="FOLLOWUP-DIAL" || eventName == "PREVIEW-DIAL"||eventName=="CRM-DIAL"){
        getCrmData();
      }
      else if(eventName =="CHANGE-STATUS"||eventName=="CRM-DISPOSE"||eventName=="CRM-HANGUP"){
        userStatusService();
      }
      else if(eventName == "DISPOSED"){
        prefs.setBool("openCrm", false);
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => HomePage()),
                (Route<dynamic> route) => false);
      }
      else if(eventName =="MONITOR-LOGOUT") {
        prefs.setBool('loginStatus', false);
        prefs.remove("listid");
        prefs.remove("_allowCamp");
        prefs.remove("selectedData");
        Url.toastShow("Agent logout from monitor.", Colors.red);
        goToIntroPage();
      }
    });
    debugPrint("AFTEREVENT");
    accessToken = prefs.getString("accessToken") ?? "";
    ipAddress = prefs.getString("ipAddress") ?? "";
    _setDial();

  }

  Future<void> getDataValue() async {
    try {
      prefs = await SharedPreferences.getInstance();
      isCallAlow =
          (prefs.getString("ManualAllow") == "Y" ? true : false) ?? false;
      strUserFullName = prefs.getString("Userfullname") ?? '';
      campId = prefs.getInt("campId") ?? 0;
      selectedListId = prefs.getInt("selectedListId") ?? 0;
      selectedSkillId = prefs.getInt("selectedSkillId") ?? 0;
      accessToken = prefs.getString("accessToken") ?? "";
      ipAddress = prefs.getString("ipAddress") ?? "";
      skillName = prefs.getString("skillName") ?? "";
      selectedSkillName = prefs.getString("selectedSkillName") ?? "";

    } catch (e) {

    }
  }


  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    slideAnimController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 400));
    slideAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(curve: Curves.ease, parent: slideAnimController));
    slideAnimController.forward();
    init();
    super.initState();
  }

  Future<void> _setDial() async {
    prefs = await SharedPreferences.getInstance();
    dispositionData = [];
    dispositionData = [
      ({'name': '', 'type': 'Select Disposition Type'})
    ];
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    txtFirstName?.dispose();
    txtAddress?.dispose();
    txtLastName?.dispose();
    slideAnimController?.dispose();
    txtConferencetime?.dispose();
    super.dispose();
  }

  // return webview and show it in external CRMPage if externalUrl != ""
  Widget webView() {
    debugPrint("SHOWWEBVIEW");
    return Visibility(
      visible: _isWebViewVisible,
      child: Expanded(
        child: WebviewScaffold(
          url: externalURL,
        ),
      ),
    );
  }

  Widget crmBody() {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 2,
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.only(left: 30, right: 30,),
            child: ListView.builder(
                itemCount: crmItemModelList.length,
                itemBuilder: (context, index) {
                  if (skipIndex != -1) {
                    skipIndex = -1;
                    return Text("");
                  }
                  if (index < crmItemModelList.length) {
                    return crmColumnItem(crmItemModelList[index], index);
                  }
                  else {
                    return Column(
                      children: <Widget>[
                        crmColumnItem(crmItemModelList[index], index),
                        SizedBox(
                          height: 130,
                        )
                      ],
                    );
                  }
                }),
          ),
        )
      ],
    );
  }

  Widget crmColumnItem(CRMItemModel crmItemModel, index) {
    crmControllerValue.add("");
    arrColumn.add(CRMColumnModel());
    skipIndex = -1;
    if(crmItemModel.type == "hidden") {
      return Text("");
    }
    else if (crmItemModel.type == "h1") {
      return Center(child: Text(crmItemModel.text ?? ""));
    }
    else if (crmItemModel.type == "span") {
      return Container(
        margin: EdgeInsets.only(bottom: 4),
        child: Text(crmItemModel.text ?? ""),
      );
    }
    else if (crmItemModel.type == "select") {
      if (crmItemModelList[index].value == null ||
          crmItemModelList[index].value == "null")
        crmItemModelList[index].value = crmItemModel.options[0].value;
      if (crmItemModel.dataColumn != "dispositionType" ||
          crmItemModel.dataColumn != "dispositionName") {
        crmControllerValue[index] = crmItemModel.value ?? "";
        arrColumn[index] = CRMColumnModel(
            name: crmItemModel.name, value: crmItemModel.value ?? "");
      }
      return Visibility(
        visible: crmItemModel.id == "hourCombo" ||
            crmItemModel.id == "minuteCombo" ? isSelectFollowUpType : true,
        child: Column(
          children: <Widget>[
            Container(
              height: 50,
              margin: EdgeInsets.only(bottom: 4,),
              padding: EdgeInsets.only(left: 15, right: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                      color: Colors.grey,
                      width: 1
                  )
              ),
              child: DropdownButton<String>(
                iconSize: 40,
                hint: Text(crmItemModel.dataColumn == "dispositionType"
                    ? "Disposition Type"
                    :
                (crmItemModel.dataColumn == "dispositionName"
                    ? "Disposition Name"
                    : crmItemModel.name ?? "")),
                underline: Container(),
                isExpanded: true,
                value: crmItemModel.dataColumn == "phone" ? selectedNumber
                    .trim() : crmItemModel.dataColumn == "dispositionType"
                    ? dispositionType
                    : (
                    crmItemModel.dataColumn == "dispositionName"
                        ? dispositionName
                        : crmItemModelList[index].value
                ),
                onChanged: (String newValue) {
                  setState(() {
                    if (crmItemModel.dataColumn == "dispositionType") {
                      if (newValue.toLowerCase().contains("followup")) {
                        isSelectFollowUpType = true;
                      }
                      else {
                        isSelectFollowUpType = false;
                      }
                      dispositionName = null;
                      dispositionType = newValue;
                      newValue = newValue.split("@")[0];
                      if (newValue.isEmpty) {
                        dispositionNameData.clear();
                        return;
                      }
                      newValue = newValue.replaceAll("[", "");
                      newValue = newValue.replaceAll("]", "");
                      dispositionNameData.clear();
                      dispositionNameData = newValue.split(",");
                    }
                    else if (crmItemModel.dataColumn == "dispositionName") {
                      dispositionName = newValue.trim();
                    }
                    else {
                      crmControllerValue[index] = newValue;
                      arrColumn[index].value = newValue;
                      crmItemModelList[index].value = newValue;
                    }
                  });
                },
                items: crmItemModel.dataColumn == "dispositionType" ?
                dispositionData.map((e) {
                  return DropdownMenuItem<String>(
                    value: e["name"].toString() + "@" + e["type"],
                    child: Text(e["type"] ?? ""),
                  );
                }).toList()
                    : (
                    crmItemModel.dataColumn == "dispositionName" ?
                    dispositionNameData.map((e) {
                      return DropdownMenuItem(
                        value: e.trim().toString(),
                        child: Text(e.toString()),
                      );
                    }).toList()
                        : (crmItemModel.dataColumn == "phone" ?
                    numbersList.map((e) {
                      return DropdownMenuItem(
                        value: e,
                        child: Text(e),
                      );
                    }).toList()
                        : crmItemModel.options.map((e) {
                      return DropdownMenuItem(
                        value: e.value ?? " ",
                        child: Text(e.text ?? ""),
                      );
                    }).toList())
                )
                ,
              ),
            ),
            crmItemModel.dataColumn == "dispositionName" &&
                isShowCustomDispoDate == true && isSelectFollowUpType == true ?
            Visibility(
              visible: isSelectFollowUpType == true ? true : false,
              child: Container(
                height: 50,
                margin: EdgeInsets.only(bottom: 4),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                        color: Colors.grey,
                        width: 1
                    )
                ),
                padding: EdgeInsets.only(left: 15, right: 5),
                child: DateTimeField(
                  decoration: InputDecoration(
                      hintText: "Select Date and Time",
                      border: InputBorder.none
                  ),
                  format: DateFormat("yyyy-MM-dd HH:mm"),
                  onShowPicker: (context, currentValue) async {
                    final date = await showDatePicker(
                        context: context,
                        firstDate: DateTime.now(),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100));
                    if (date != null) {
                      final time = await showTimePicker(
                        context: context,
                        initialTime:
                        TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                      );
                      ShowCustomDispoDate =
                          DateFormat("yyyy-MM-dd HH:mm").format(
                              DateTimeField.combine(date, time));
//                  crmControllerValue[index] = date.toIso8601String();
                      return DateTimeField.combine(date, time);
                    } else {
                      return currentValue;
                    }
                  },
                ),
              ),
            )
                : Container()
          ],
        ),
      );
    }
    else if (crmItemModel.type == "date") {
      arrColumn[index].value = crmItemModel.value ?? "";
      arrColumn[index] = CRMColumnModel(
          name: crmItemModel.name, value: crmItemModel.value ?? "");
      return Visibility(
        visible: crmItemModel.id == "followupDateText"
            ? isSelectFollowUpType
            : true,
        child: Container(
          height: 50,
          margin: EdgeInsets.only(bottom: 4),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: Border.all(
                  color: Colors.grey,
                  width: 1
              )
          ),
          padding: EdgeInsets.only(left: 15, right: 5),
          child: DateTimeField(
            decoration: InputDecoration(
                hintText: "Select Date",
                border: InputBorder.none
            ),
            format: DateFormat("yyyy-MM-dd"),
            onShowPicker: (context, currentValue) async {
              final date = await showDatePicker(
                  context: context,
                  firstDate: DateTime.now(),
                  initialDate: currentValue ?? DateTime.now(),
                  lastDate: DateTime(2100));
              if (date != null) {
                arrColumn[index].value = DateFormat("yyyy-MM-dd").format(date);
                crmItemModelList[index].value =
                    DateFormat("yyyy-MM-dd").format(date);
//                  crmControllerValue[index] = date.toIso8601String();
                return date;
              } else {
                return currentValue;
              }
            },
          ),
        ),
      );
    }
    else if (crmItemModel.type == "button" || crmItemModel.type == "submit") {
      return Container(
        height: 50,
        margin: EdgeInsets.symmetric(vertical: 20),
        child: RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8)
          ),
          onPressed: () {
            if (crmItemModel.id == "form_submit") {
              _dispositionWebService();
            }
          },
          child: Text(crmItemModel.value ?? "", style: TextStyle(
              color: Colors.white,
              fontSize: 20
          ),),
        ),
      );
    }
    else if (crmItemModel.type == "textarea") {
      crmControllerValue[index] = crmItemModel.value ?? "";
      arrColumn[index] = CRMColumnModel(
          name: crmItemModel.name, value: crmItemModel.value ?? "");
      return Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            border: Border.all(
                color: Colors.grey.shade500
            )
        ),
        margin: EdgeInsets.only(bottom: 4),
        height: 100,
        child: TextField(
          onChanged: (value) {
            crmControllerValue[index] = value;
            arrColumn[index].value = value;
            crmItemModelList[index].value = value;
          },
          decoration: InputDecoration.collapsed(
              hintText: crmItemModel.placeholder ?? "Enter Text Here..."),
          maxLines: 4,
        ),
      );
    }
    else if (crmItemModel.type == "radio") {
      skipIndex = index + 1;
      return Row(
        children: <Widget>[
          Radio(
            value: crmItemModel.defaultValue,
            groupValue: groupValue,
            onChanged: (value) {
              if (crmItemModel.dataColumn == "gender") {
                genderMap = {
                  "name": crmItemModel.name,
                  "type": crmItemModel.type,
                  "value": crmItemModel.value
                };
              }
            },
          ),
          Text(crmItemModelList[index + 1].text)
        ],
      );
    }
    else if (crmItemModel.type == "text") {
      crmControllerValue[index] = crmItemModel.value ?? "";
      arrColumn[index] = CRMColumnModel(
          name: crmItemModel.name, value: crmItemModel.value ?? "");
      TextEditingController controller = TextEditingController();
      controller.text =
      crmItemModel.value == null || crmItemModel.value == "null"
          ? ""
          : crmItemModel.value;
      return Container(
          height: 50,
          padding: EdgeInsets.symmetric(horizontal: 8),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: Border.all(
                  width: 1,
                  color: Colors.grey
              )
          ),
          margin: EdgeInsets.only(bottom: 4,),
          child: TextField(
            onChanged: (value) {
              crmControllerValue[index] = (value);
              arrColumn[index].value = value;
              crmItemModelList[index].value = value;
            },
            controller: controller,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintText: crmItemModel.placeholder ?? ""
            ),
          ));
    }
    else if (crmItemModel.type == "input") {
      return Container(
        height: 50,
        child: TextField(

        ),
      );
    }
  }

  Future<bool> _onWillPop() {
    if (!slideAnimation.isCompleted) {
      isShowDialPad = false;
      slideAnimController.forward().orCancel;
      setState(() {

      });
      return Future.value(false);
    }
    return Future.value(true);

  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: buildBody(),
    );

  }

  buildBody(){
    if (isShowUI == false) {
      return Scaffold(
        body: Center(
            child: Container(
                width: 30, height: 30, child: CircularProgressIndicator())),
      );
    }

    if (externalURL.trim() != "") {
      return Stack(
        children: [
          Scaffold(
              floatingActionButton:_isWebViewVisible == false ? FloatingActionButton(
                heroTag: ValueKey(random.nextInt(300)),
                backgroundColor: Color(0xff00476d),
                child:Icon(isShowDialPad == false ? Icons.dialpad_rounded
                    : Icons.close
                  ,size: 30,),
                onPressed: (){
                  debugPrint("CLICK");
                  isShowDialPad = !isShowDialPad;
                  if (slideAnimation.isCompleted) {
                    slideAnimController.reverse().orCancel;
                  } else {
                    slideAnimController.forward().orCancel;
                  }
                  setState(() {

                  });
                },
              ) : null,
              backgroundColor: Colors.white,
              body: CenteredWidget(
                child: Stack(
                  children: <Widget>[
                    Container(
                      height: 95,
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                            stops: [0.4, 0.8, 1],
                            colors: [
                              // Colors are easy thanks to Flutter's Colors class.
                              Color(0xff00476d),
                              Color(0xff007981),
                              Color(0xff00a969),
                            ],
                          ),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(30),
                              bottomRight: Radius.circular(30))),
                      child: Container(
                        margin: EdgeInsets.only(top: 35, left: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              width: 30,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  height: 30,
                                  child: Text(
                                    'External CRM',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontFamily: 'roboto-regular'),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.only(left: 0),
                                  child: Text(userStatus ?? "",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),),
                                ),
                              ],
                            ),
                            Spacer(),
                            Material(
                              color: Colors.transparent,
                              child: IconButton(icon: Icon(Icons.refresh_rounded,size: 30,
                              color: Colors.white,),
                                  onPressed: (){
                                    userStatusService();
                                  }),
                            ),
                            Container(
                              width: 60,
                              height: 60,
                              padding: EdgeInsets.only(right: 20, bottom: 10),
                              child: FloatingActionButton(
                                heroTag: ValueKey(random.nextInt(300)),
                                onPressed: () {
                                  setState(() {
                                    _isWebViewVisible = !_isWebViewVisible;
                                  });
                                },
                                child: Image.asset(
                                  "assets/2.0x/callend.png",
                                  width: 40,
                                  height: 40,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 95),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          _isWebViewVisible == true
                              ? webView()
                              : Expanded(
                              child: crmBody()
                          ),
                        ],
                      ),
                    ),
                    isShowProgressBar == true
                        ? Center(
                          child: Container(
                        width: 30,
                        height: 30,
                        child: CircularProgressIndicator(),
                      ),
                    )
                        : Text(""),

                    Visibility(
                        visible: !_isWebViewVisible,
                        child: showDialer(MediaQuery.of(context).size.height,txtNubmber )
                    )

                  ],
                ),
              )),
          //showDialer(MediaQuery.of(context).size.height,txtNubmber )
        ],
      );
    } else {
      return new WillPopScope(
          onWillPop: () {
            return Future.value(false);
          },
          child: Scaffold(
            floatingActionButton: FloatingActionButton(
              heroTag: ValueKey(random.nextInt(300)),
              backgroundColor: Color(0xff00476d),
              child:Icon(isShowDialPad == false ? Icons.dialpad_rounded
                  : Icons.close
                ,size: 30,),
              onPressed: (){
                isShowDialPad = !isShowDialPad;
                if (slideAnimation.isCompleted) {
                  slideAnimController.reverse().orCancel;
                } else {
                  slideAnimController.forward().orCancel;
                }
                setState(() {

                });
              },
            ) ,
            backgroundColor: Colors.white,
            body: Stack(
              children: [
                Column(
                  children: <Widget>[
                    Container(
                      height: 95,
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                            stops: [0.4, 0.8, 1],
                            colors: [
                              // Colors are easy thanks to Flutter's Colors class.
                              Color(0xff00476d),
                              Color(0xff007981),
                              Color(0xff00a969),
                            ],
                          ),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(30),
                              bottomRight: Radius.circular(30))),
                      child: Container(
                        margin: EdgeInsets.only(top: 35, left: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Column(
                             crossAxisAlignment : CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  alignment: Alignment.center,
                                  height: 30,
                                  child: Text(
                                    "Internal CRM",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontFamily: 'roboto-regular'),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Text(userStatus ?? "",
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                )
                              ],
                            ),
                            Spacer(),
                            Material(
                              color: Colors.transparent,
                              child: IconButton(icon: Icon(Icons.refresh_rounded,
                               color: Colors.white,size: 30,
                              ),
                                  onPressed: (){
                                    userStatusService();
                                  }),
                            ),
                            SizedBox(width: 15,)

                          ],
                        ),
                      ),
                    ),
                    Expanded(child: crmBody()),
                  ],
                ),
                showDialer(MediaQuery.of(context).size.height,txtNubmber )
              ],
            ),
          ));
    }
  }

  Future<void> _dispositionWebService() async {
    String FOLLOWUPDate = "",
        FOLLOWUPHours = "",
        FOLLOWUPMinute = "";
    String FOLLOWUPDateTime = "";
    var arrCRMColumns = [];
    isShowUI = false;
    isShowProgressBar = true;
    if (this.mounted) {
      setState(() {});
    }


    if (crmControllerValue.isNotEmpty || crmControllerValue.length > 0) {
      for (var i = 0; i < crmItemModelList.length; i++) {
        if (crmItemModelList[i].value != null &&
            crmItemModelList[i].value != "submit" &&
            crmItemModelList[i].value != "button"
            && crmItemModelList[i].name != "dispositionName" &&
            crmItemModelList[i].name != "dispositionType"
        ) {
          if (crmItemModelList[i].id == "followupDateText") {
            FOLLOWUPDate = crmItemModelList[i].value ?? "";
          }
          else if (crmItemModelList[i].id == "hourCombo") {
            FOLLOWUPHours = crmItemModelList[i].value ?? "";
          }
          else if (crmItemModelList[i].id == "minuteCombo") {
            FOLLOWUPMinute = crmItemModelList[i].value ?? "";
          }
          else {
            if (crmItemModelList[i].name == "phone") {
              arrCRMColumns.add({
                "name": crmItemModelList[i].name,
                "value": selectedNumber,
                "type": "String"
              });
              continue;
            }
            arrCRMColumns.add({
              'name': crmItemModelList[i].name,
              'value': crmItemModelList[i].value == null ||
                  crmItemModelList[i].value == "null"
                  ? ""
                  : (crmItemModelList[i].dataType == "int" ? int.parse(
                  crmItemModelList[i].value) : (
                  crmItemModelList[i].dataType == "double" ? double.parse(
                      crmItemModelList[i].value) : crmItemModelList[i].value
                      .toString())
              ),
              'type': crmItemModelList[i].dataType ?? "String"
            });
          }
        }
      }
    }

    String dispoType;
    if (dispositionType != null) {
      dispoType = dispositionType.split("@")[1];
    }


    if (dispoType != null && dispoType.isNotEmpty) {
      if (dispoType == "FOLLOWUP") {
        if (dispositionName == null || dispositionName.isEmpty) {
          Fluttertoast.showToast(msg: "Select Disposition Name",
              backgroundColor: Colors.red, textColor: Colors.white
          );
          return;
        }
        if (isShowCustomDispoDate == false) {
          if (FOLLOWUPDate.isEmpty) {
            Fluttertoast.showToast(msg: "Select Disposition Date",
                backgroundColor: Colors.red, textColor: Colors.white
            );
            return;
          }
          else if (FOLLOWUPHours.isEmpty) {
            Fluttertoast.showToast(msg: "Select Disposition Hour",
                backgroundColor: Colors.red, textColor: Colors.white
            );
            return;
          }
          else if (FOLLOWUPMinute.isEmpty) {
            Fluttertoast.showToast(msg: "Select Disposition Minute",
                backgroundColor: Colors.red, textColor: Colors.white
            );
            return;
          }
          FOLLOWUPDateTime =
              FOLLOWUPDate + " " + FOLLOWUPHours + ":" + FOLLOWUPMinute + ":00";
        }
        else {
          FOLLOWUPDateTime = ShowCustomDispoDate + ":00";
        }
      }
    }

    String body;

    String formattedStartTime;
    prefs = await SharedPreferences.getInstance();
    var token = (prefs.getString('accessToken') ?? '');
    String iPAddress = prefs.getString("ipAddress") ?? '';
    var connectivityResult = await (new Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      HttpOverrides.global = new MyHttpOverrides();

      if (dispoType == "FOLLOWUP" || dispoType == "APPOINTMENT") {
        if (FOLLOWUPDateTime == null) {
          Url.toastShow("Select Start Date And Time");
        } else {
          DateTime dispoDateTime = DateFormat("yyyy-MM-dd HH:mm:ss").parse(
              FOLLOWUPDateTime);

          if (dispoDateTime.millisecondsSinceEpoch < DateTime
              .now()
              .millisecondsSinceEpoch) {
            Url.toastShow("Please select valid date and time.");
            return;
          }
          body = jsonEncode({
            "type": "APP-REQUEST",
            "data": {
              "disposition_type": dispoType,
              "disposition_name": dispositionName,
              "crm_column": arrCRMColumns,
              'followupdate': FOLLOWUPDateTime
            }
          });

          submitData(iPAddress, body, token);
          return;
        }
      }
      else {
        if (dispositionName == null || dispositionName == "") {
          body = jsonEncode({
            "type": "APP-REQUEST",
            "data": {
              "crm_column": arrCRMColumns
            }
          });
          submitData(iPAddress, body, token);
          return;
        }
        else {
          body = jsonEncode({
            "type": "APP-REQUEST",
            "data": {
              "disposition_type": dispoType,
              "disposition_name": dispositionName,
              "crm_column": arrCRMColumns
            }});
          submitData(iPAddress, body, token);
          return;
        }
      }
    }
    else {
      Url.toastShow("Kindly check your internet connection!!");
    }
  }

  Future<void> submitData(String iPAddress, String body, String token) async {
    prefs = await SharedPreferences.getInstance();
    try {

      await http.post(iPAddress + url.crm, body: body, headers: {
        "Accept": "application/json",
        "content-type": "application/json",
        "Authorization": token
      }).then((response) async {
        String responseBody = response.body;
        var responseJSON = json.decode(responseBody);
        debugPrint("Disposition Data : $responseJSON");
        if (response.statusCode == 200) {
          if (responseJSON["response"] == "SUCCESS") {
            if (dispositionType == null ||
                dispositionType == "Select Disposition Type") {
              //Url.toastShow("Please Refresh ! try again ...", Colors.green);
              setState(() {
                getCrmData();
              });
            } else {
              prefs.setBool("openCrm", false);
              Fluttertoast.showToast(
                  msg: "Disposed successfully",
                  textColor: Colors.white,
                  backgroundColor: Colors.green);
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => HomePage()),
                      (Route<dynamic> route) => false);
            }
          }
        }
        else if(responseJSON["response"] == "FAILURE" &&
            responseJSON["error"]["code"] == 200){
          Url.toastShow(responseJSON["error"]["message"]);
          debugPrint("I'm HERE");
          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
              builder: (context) => HomePage()
          ), (route) => false);
        }
        else if(responseJSON["response"] == "FAILURE" &&
            responseJSON["error"]["code"] == 201){
          Url.toastShow(responseJSON["error"]["message"]);
          debugPrint("I'm HERE");
          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
              builder: (context) => HomePage()
          ), (route) => false);
        }
        else if (responseJSON["response"] == "FAILURE" &&
            (responseJSON["error"]["message"] == "Authorization Failure")) {
          prefs.setBool('loginStatus', false);
          // prefs.remove("dialCode");
          prefs.remove("listid");
          prefs.remove("_allowCamp");
          prefs.remove("selectedData");
          Url.toastShow(responseJSON["error"]["message"], Colors.red);
          goToIntroPage();
        }
        else {
          isShowProgressBar = false;
          setState(() {});
          Url.toastShow(responseJSON["error"]["message"], Colors.red);
        }
      }).timeout(const Duration(seconds: 30));
    }
    on Exception catch (error) {
      debugPrint("Error : $error");
    }
  }

  getHtmlCRMData(jsonData) async {
    try{
      isSelectFollowUpType = false;
      var dataVal = jsonData;
      dispositionType = null;
      dispositionName = null;
      debugPrint("CRMHTMlDAta $jsonData");

      crmValuesList.clear();
      Map<String, dynamic> crmValues = dataVal["crm_value"];

      Map<String, dynamic> callStartUrl = dataVal["call_start_url"] == null ||
          dataVal["call_start_url"] == "" ? null :
      dataVal["call_start_url"];
      if (callStartUrl != null && callStartUrl != "") {
        externalURL = callStartUrl["api_url"] ?? "";
      }

      numbersList.clear();
      if (crmValues != null) {
        crmValues.forEach((key1, v) {
          if (key1 == "dialing_number") {
            selectedNumber = v;
            debugPrint("dialing_number $selectedNumber");
          }
          if (key1.toLowerCase().contains("phone") &&
              key1.toLowerCase().substring(0, 5).contains("phone")) {
            if (v != "" && v != " ") {
              numbersList.add(v.trim().toString());
            }
          }
          else {
            crmValuesList[key1.toString().toLowerCase()] = v;
          }
        }
        );
      }


      // ********* DispositionValues **********

      crmItemModelList = [];
      dispositionData.clear();
      Map<String, dynamic> dispoData = dataVal["dispositions"] == null ||
          dataVal["dispositions"] == "" ?
      null : dataVal["dispositions"];
      if (dispoData != null) {
        dispoData.forEach((k, v) {
          dispositionData.add(
              {
                "type": k,
                "name": v
              }
          );
        });
      }

      // ************************************

      bool isDispoExit = false;
      Map<String, dynamic> crmData = dataVal["crm"];
      crmItemModelList = new List<CRMItemModel>();
      if (crmData != null) {
        crmData.forEach((k, v) {
          for (var item in v) {
            CRMItemModel crmItemModel = new CRMItemModel();
            crmItemModel.options = new List();
            item.forEach((key, value) {
//            type,text,id,name,style,placeholder
              if (key == "id") {
                if (value == "dispositionNameCombo") {
                  isDispoExit = true;
                }
                if (value == "hourCombo") {
                  isShowCustomDispoDate = false;
                }
                crmItemModel.id = value ?? "";
              } else if (key == "type") {
                crmItemModel.type = value ?? "";
              }
              else if (key == "data-column") {
                crmItemModel.dataColumn = value ?? "";
                if (crmItemModel.value == null || crmItemModel.value.isEmpty) {
                  crmItemModel.value = crmValuesList[value].toString() ?? "";
                }
                crmItemModel.dataType = crmValuesList[value] is int
                    ? "int"
                    : (crmValuesList[value] is double ? "double" : "String");
                if (v == "phone") {
                  crmItemModel.value = selectedNumber;
                }
              }
              else if (key == "value") {
                crmItemModel.value = value ?? "";
              }
              else if (key == "text") {
                crmItemModel.text = value ?? "";
              } else if (key == "name") {
                crmItemModel.name = value ?? "";
              }
              else if (key == "placeholder") {
                crmItemModel.placeholder = value ?? "";
              } else if (key == "options") {
                for (Map<String, dynamic> optionItem in value) {
                  OptionItem optionItemModel = OptionItem();
                  optionItem.forEach((itemKey, itemValue) {
                    if (itemKey == "text") {
                      optionItemModel.text = itemValue ?? "";
                    }
                    else if (itemKey == "value") {
                      optionItemModel.value = itemValue ?? "";
                    }
                    else if (itemKey == "id") {
                      optionItemModel.id = itemValue ?? "";
                    }
                  });
                  crmItemModel.options.add(optionItemModel);
                }
              }
            });
            crmItemModelList.add(crmItemModel);
          }
        });
      }

      if (isDispoExit == false) {
        crmItemModelList.add(CRMItemModel(
            type: "select",
            id: "dispositionTypeCombo",
            name: "dispositionType",
            dataColumn: "dispositionType",
            options: [
              OptionItem(
                  text: "SELECT",
                  value: "",
                  id: "SELECT"
              )
            ]));
        crmItemModelList.add(CRMItemModel(
            type: "select",
            id: "dispositionNameCombo",
            name: "dispositionName",
            dataColumn: "dispositionName",
            options: [
              OptionItem(
                  text: "SELECT",
                  value: "",
                  id: "SELECT"
              )
            ]));
      }

    }catch(e) {
      debugPrint("Exception : (getHtmlCRMData) -> $e");
    }
    debugPrint("FINAL Json $crmItemModelList");
    if (this.mounted) {
      setState(() {

      });
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    debugPrint("Application State :  $state");
    checkCallState(state);
  }

  Future<void> checkCallState(state) async {
    prefs = await SharedPreferences.getInstance();
    if (state == AppLifecycleState.resumed) {
      isShowDialog = true;
       showLoadingDialog();
      await userStatusService();
      isShowDialog = false;
      showLoadingDialog();
      setState(() {

      });
    }
  }

  showLoadingDialog() {
    if(isShowDialog == true) {
      showDialog(context: context,
          child: AlertDialog(
            content: Row(
              children: [
                CircularProgressIndicator(),
                SizedBox(width: 15,),
                Text("Loading...")
              ],
            ),
          )
      );
    }
    else {
      if(Navigator.canPop(context)) {
        Navigator.pop(context);
      }
    }
  }

  Future<void> userStatusService() async {
    var connectivityResult = await (new Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      HttpOverrides.global = new MyHttpOverrides();

      ipAddress = prefs.getString("ipAddress") ?? "";
      accessToken = prefs.getString("accessToken") ?? "";
      try {
        debugPrint(
            "Call usetStatus , Token -> $accessToken , Ip -> $ipAddress");
        await http.post(ipAddress + url.checkCallStatus, headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "Authorization": accessToken
        }).then((response) async {
          String responseBody = response.body;
          var responseJSON = json.decode(responseBody);
          debugPrint("Response : $responseJSON");
          if (response.statusCode == 200) {
            if (responseJSON["response"] == "SUCCESS") {
              var arrData = responseJSON["data"];
              var list = arrData;
              Map myMap = list;
              userStatus = myMap["status"] ?? "";
              if (myMap["status"] == "AUX") {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => Setting()));
              }
              else{
              if (arrData["status"] == "INCALL" || arrData["status"] == "ACW" ||
                  arrData["status"] == "DIAL"
                  || arrData["status"] == "FOLLOWUP" ||
                  arrData["status"] == "QUEUE") {
                if (myMap["status"] == "ACW") {
                  userStatus = "Customer Down";
                }
                if (arrData["account_code"] != null &&
                    arrData["account_code"] != '') {
                  String currentAccountCode = prefs.getString("accountCode");
                  debugPrint("PreviousAccountCode  $currentAccountCode");
                  debugPrint("AccountCode  ${arrData["account_code"]}");
                  if(!currentAccountCode.toString().trim().contains(arrData["account_code"].toString().trim())) {
                    prefs.setString("accountCode", arrData["account_code"]);
                  }
                }
                getCrmData();
                // prefs.setBool("openCrm", false);
              } else {
                Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder:
                    (context) => HomePage()
                ), (route) => false);
              }
            }
              if(this.mounted) {
                setState(() {

                });
              }
            }
            else if (responseJSON["response"] == "FAILURE" &&
                responseJSON["error"]["message"] == "User active from web") {
              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                  builder: (context) => HomePage()
              ), (route) => false);
            }
          }
          else if (responseJSON["response"] == "FAILURE" &&
              (responseJSON["error"]["message"] == "Authorization Failure")) {
            prefs.setBool('loginStatus', false);
            // prefs.remove("dialCode");
            prefs.remove("listid");
            prefs.remove("_allowCamp");
            prefs.remove("selectedData");
            Url.toastShow(responseJSON["error"]["message"], Colors.red);
            goToIntroPage();
          }
          else if (response.statusCode == 401) {
            prefs.setBool("loginStatus", false);
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                builder: (context) => HomePage()
            ), (route) => false);
          } else {
            Url.toastShow(responseJSON["message"], Colors.red);
          }
        }).timeout(Duration(seconds: 5));
      } on Exception catch (error) {
        // When Service is not Working
        debugPrint("Error : $error");
      }
    }
  }

  Future<void> getCrmData() async {
    isShowUI = false;
    isShowProgressBar = true;
    if (this.mounted) {
      setState(() {});
    }
    debugPrint("Call -> getCRMDATA");
    prefs = await SharedPreferences.getInstance();
    var connectivityResult = await (new Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      HttpOverrides.global = new MyHttpOverrides();
      try {
        await http.get(ipAddress + url.crm, headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "Authorization": accessToken
        }).then((response) async {
          String responseBody = response.body;
          debugPrint("CRM BODY ${responseBody}");
          var responseJSON = json.decode(responseBody);
          if (response.statusCode == 200){
            _controllers = [];
            arrColumn = [];
            if (responseJSON["response"] == "SUCCESS") {
              var list = responseJSON["data"];
              getHtmlCRMData(list);
            }
            else {
              if (this.mounted) {
                isShowUI = true;
                isShowProgressBar = false;
                setState(() {});
              }
              Url.toastShow(responseJSON["error"]["message"], Colors.red);
              return;
            }
          }
          else if (responseJSON["response"] == "FAILURE" &&
              (responseJSON["error"]["message"] == "Authorization Failure")) {
            prefs.setBool('loginStatus', false);
            // prefs.remove("dialCode");
            prefs.remove("listid");
            prefs.remove("_allowCamp");
            prefs.remove("selectedData");
            Url.toastShow(responseJSON["error"]["message"], Colors.red);
            goToIntroPage();
          }
          else if (responseJSON["response"] == "FAILURE" &&
              responseJSON["error"]["message"] == "User active from web") {
            Url.toastShow(responseJSON["error"]["message"]);
          }
          else if(responseJSON["response"] == "FAILURE") {
            Url.toastShow(responseJSON["error"]["message"]);
          }
          else {
            Url.toastShow("getting some error");
          }
        }).timeout(const Duration(seconds: 10));
      } on Exception catch (error) {}
    } else {}
    isShowUI = true;
    isShowProgressBar = false;
    if (this.mounted) {
      setState(() {});
    }
  }

  void goToIntroPage() {
    prefs.setBool("setCampDetails", false);
    prefs.setBool("loginStatus", false);
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => Login()),
            (Route<dynamic> route) => false);
  }


  Future<void> dialCallService() async {
    String listId;
    debugPrint("inside[] -> dialCallService , user_no ->  $userPhoneNo");
    userPhoneNo = userPhoneNo.trim();
    userPhoneNo = userPhoneNo.replaceAll("-", "");
    if (userPhoneNo == null || userPhoneNo.isEmpty || userPhoneNo == "") {
      Fluttertoast.showToast(msg: "Enter a valid number");
      return;
    } else {
      phoneNumber = prefs.getString("ServerPhoneNumber") ?? '';
      debugPrint(
          "dialCall [] ->  userno $userPhoneNo ,campId $campId , skill_name $skillName");
      debugPrint("server phone no $phoneNumber");
      var connectivityResult = await (new Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.wifi ||
          connectivityResult == ConnectivityResult.mobile) {
        HttpOverrides.global = new MyHttpOverrides();
        var body = jsonEncode({
          "type": "APP-REQUEST",
          "data": {
            "camp_id": campId,
            "skill_id": selectedSkillId,
            "phone_number": userPhoneNo,
            "list_id": selectedListId
          }
        });

        accessToken = prefs.getString("accessToken") ?? "";

        debugPrint(
            "CALL-API (MANUAL-DIAL) : body - ${body} , ip- ${ipAddress + url.dialCall} ,accessToken $accessToken ");
        //return;
        try {
          await http.post(ipAddress + url.dialCall, body: body, headers: {
            "Accept": "application/json",
            "content-type": "application/json",
            "Authorization": accessToken
          }).then((response) async {
            debugPrint("ManualApiResponse ${response.body}");
            var responseJSON = json.decode(response.body);
            if (response.statusCode == 200) {
              String responseBody = response.body;
//              var responseJSON = json.decode(responseBody);
              debugPrint("dialCallService() res-> ${responseJSON}");
              if (responseJSON["response"] == "SUCCESS") {
                String failType = responseJSON["data"]["fail_type"] ?? "";
                if(failType!="") {
                  if(failType =="FAIL"||failType=="TRUNKFAIL") {
                    //  showManulDialFailDailog(msg : "this number is in follow up.");
                    prefs.setBool("openCrm", true);
                    prefs.setString("dialNumber", userPhoneNo);
                    prefs.setString("CRM", "CRM");
                    prefs.setBool("is_init_call", true);
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context)=>CRMPage()
                    ));
                    debugPrint("DialCallSER isCAllI $isCallInitiate");
                  }
                  else if(responseJSON["response"] == "FAILURE") {
                    Url.toastShow(responseJSON["error"]["message"]);
                  }
                  else {
                    Fluttertoast.showToast(msg: "getting some error",
                        backgroundColor: Colors.red,textColor: Colors.white
                    );
                  }
                }
                else {
                  prefs.setBool("openCrm", true);
                  prefs.setString("dialNumber", userPhoneNo);
                  prefs.setString("CRM", "CRM");
                  prefs.setBool("is_init_call", true);
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context)=>CRMPage()
                  ));
                  debugPrint("DialCallSER isCAllI $isCallInitiate");
                }
                isCallInitiate = false;
                setState(() {

                });
              }
              else if(responseJSON["response"] == "FAILURE"&&responseJSON["error"]["message"]=="User active from web"){
                Url.toastShow(responseJSON["error"]["message"]);
              }
              else if(responseJSON["response"] == "FAILURE" && responseJSON["error"]["code"] ==100 ){
                Fluttertoast.showToast(msg: "Call added into Queue.",
                backgroundColor: Colors.green
                );
              }
              else {
                isCallInitiate = false;
                setState(() {});
              }
            }
            else if (responseJSON["response"] == "FAILURE" &&
                (responseJSON["error"]["message"] == "Authorization Failure")){
              Fluttertoast.showToast(msg:responseJSON["error"]["message"],
                  backgroundColor: Colors.red,textColor: Colors.white
              );
            }
            else if(responseJSON["response"] == "FAILURE" && responseJSON["error"]["code"] ==100 ){
              Fluttertoast.showToast(msg: responseJSON["error"]["message"],
                  backgroundColor: Colors.green
              );
            }
            else if (responseJSON["response"] == "FAILURE") {
              Url.toastShow(responseJSON["error"]["message"] ?? "");
              isCallInitiate = false;
              setState(() {});
            }
            else {
              isCallInitiate = false;
              String errorMsg = responseJSON["error"]["message"]??"";
              if(errorMsg !=null || errorMsg !=""){
                Url.toastShow(errorMsg);
              }
              else {
                Url.toastShow("please try again.");
              }
              setState((){});}
          }).timeout(const Duration(seconds: 10));
        } on Exception catch (error) {
          isCallInitiate = false;
          setState(() {});
          loader = false;
        }
      } else {
        Url.toastShow("Kindly check your internet connection!!");
      }
    }
  }

  showDialer(_height,txtNumer) {
  return  AnimatedBuilder(
      animation: slideAnimController,
      child: Container(
        margin: EdgeInsets.only(top: _height*0.30),
        // padding: const EdgeInsets.only(left: 20,right:20),
        height: MediaQuery.of(context).size.height ,
        child: Card(
          child: Container(
            decoration: BoxDecoration(
              // color: Colors.blue,
                border: Border(
                  top: BorderSide(
                    color: Color(0xff00476d),
                  ),
                )
            ),
            margin: EdgeInsets.only(top: 0),
            child: Column(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                        bottom: BorderSide(
                          color:Color(0xff00476d),
                        ),

                      )
                  ),
                  height: 60,
                  width: MediaQuery.of(context).size.width,
                  child: new Row(
                    children: <Widget>[
                      SizedBox(width: 10,),
                      IconButton(
                          icon: Image.asset(
                            'assets/2.0x/contact-book.png',
                            color: Color(0xff00476d),
                            width: 30,
                            height: 30,
                          ) ,
                          onPressed: () async{
                            final PhoneContact contact =
                                await FlutterContactPicker.pickPhoneContact();
                            debugPrint("PICKCONTACT ${contact.phoneNumber.number}");
                            txtNubmber.clear();
                            txtNubmber.text = contact.phoneNumber.number;
                          }),
                      Expanded(
                        child: InkWell(
                          onLongPress: (){
                            FocusScope.of(context).requestFocus(FocusNode());
                          },
                          child: new TextFormField(
                            onTap: (){
                              FocusScope.of(context).requestFocus(FocusNode());
                            },
                            enableInteractiveSelection: true,
                            enabled: false,
                            controller: txtNumer,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(20),
                            ],
                            style: const TextStyle(
                                color: Color(0xff62635f),
                                fontFamily: "roboto-regular",
                                fontSize: 25),
                            textAlign: TextAlign.center,
                            decoration: new InputDecoration(
                              fillColor: Colors.white,
                              border: InputBorder.none,
                              hintStyle: const TextStyle(
                                  color: Color(0xffa4a9aa),
                                  fontFamily: "roboto-regular",
                                  fontSize: 25),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                            top: 0.0, right: 15.0),
                        height: 30,
                        width: 30,
                        decoration: new BoxDecoration(),
                        child: new GestureDetector(
                          onTap: () {
                            if (txtNumer.text.isNotEmpty) {
                              txtNumer.text = txtNumer.text.substring(
                                  0, txtNumer.text.length - 1);
                              setState(() {});
                            }
                          },
                          onLongPress: () {
                            if (txtNumer.text.isNotEmpty) {
                              txtNumer.text = "";
                              setState(() {});
                            }
                          },
                          child: Image.asset(
                            'assets/2.0x/backspace.png',
                            fit: BoxFit.cover,
                            color:Color(0xff00476d),
                          ),
                        ),
                      ),
                      //your elements here
                    ],
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: Container(
                    padding:
                    EdgeInsets.only(top: 20, left: 10, right: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceAround,
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment:
                              CrossAxisAlignment.center,
                              children: <Widget>[
                                _numberButtonItem("1"),
                                _numberButtonItem("2"),
                                _numberButtonItem("3"),
                              ]),
                        ),
                        Expanded(
                          flex: 2,
                          child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment:
                              MainAxisAlignment.spaceAround,
                              crossAxisAlignment:
                              CrossAxisAlignment.center,
                              children: <Widget>[
                                _numberButtonItem("4"),
                                _numberButtonItem("5"),
                                _numberButtonItem("6"),
                              ]),
                        ),
                        Expanded(
                          flex: 2,
                          child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment:
                              MainAxisAlignment.spaceAround,
                              crossAxisAlignment:
                              CrossAxisAlignment.center,
                              children: <Widget>[
                                _numberButtonItem("7"),
                                _numberButtonItem("8"),
                                _numberButtonItem("9"),
                              ]),
                        ),
                        Expanded(
                          flex: 2,
                          child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment:
                              MainAxisAlignment.spaceAround,
                              crossAxisAlignment:
                              CrossAxisAlignment.center,
                              children: <Widget>[
                                _numberButtonItem("*"),
                                _numberButtonItem("0"),
                                _numberButtonItem("#"),
                              ]),
                        ),
                        Expanded(
                          flex: 3,
                          child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment.center,
                              crossAxisAlignment:
                              CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  margin:
                                  const EdgeInsets.only(top: 10.0),
                                  height: 70,
                                  width: 70,
                                  decoration: new BoxDecoration(
                                      color: Color(0xff399933),
                                      borderRadius:
                                      BorderRadius.circular(40)),
                                  child: ButtonTheme(
                                    child: new FlatButton(
                                      child: Image.asset(
                                        'assets/2.0x/call.png',
                                        color: Colors.white,
                                        width: 30,
                                        height: 30,
                                      ),
                                      onPressed: () {
                                        userPhoneNo =
                                            txtNumer.text.toString();
                                        dialCallService();
                                      },
                                    ),
                                  ),
                                ),
                              ]),
                        ),
                        SizedBox(
                          height: 10,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      builder: (BuildContext context, Widget child) {
        return Transform(
          transform:  Matrix4.translationValues(
              0.0,
              _height *
                  (slideAnimation != null
                      ? slideAnimation.value
                      : 1.0),
              0.0),
          child: child,
        );
      },
    );
  }

  Widget _numberButtonItem(String numberText) {
    return Container(
        color: Colors.white,
        height: 50,
        width: 100,
        child: Material(
          color: Colors.white,
          child: InkWell(
            onTap: () {
              txtNubmber.text += numberText;
            },
            child: Container(
              alignment: Alignment.center,
              child: Text(
                numberText,
                textAlign: TextAlign.start,
                style: TextStyle(
                    color: Color(0xff686966),
                    fontFamily: 'roboto-regular',
                    fontSize: 40,
                    fontWeight: FontWeight.w100),
              ),
            ),
          ),
        ));
  }

}
