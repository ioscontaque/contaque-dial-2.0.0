class CallDetailModel{
  final duration , date , phoneNumber ;

  CallDetailModel({
    this.date,
    this.phoneNumber,
    this.duration
  });
}