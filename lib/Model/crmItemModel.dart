class CRMItemModel {
  String type,text,id,name,style,placeholder,value,dataColumn;
  String dataType;
  String defaultValue;
  List<OptionItem> options = new List<OptionItem>();
  CRMItemModel({this.id,this.text,this.style,this.name,this.type,this.options ,this.placeholder,this.value,this.dataColumn});


}

class OptionItem {
  String text,value,id;
  OptionItem({this.text,this.id,this.value});
}