class SkillDetailModel{
  final String name ;
  final int id;

  SkillDetailModel({
    this.name,
    this.id,
  });
}