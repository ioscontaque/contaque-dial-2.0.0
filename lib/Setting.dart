import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:dialer/HomePage/HomePage.dart';
import 'package:dialer/custom_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Login/Login.dart';
import 'Model/AllowCampaign.dart';
import 'Model/ListDetailModel.dart';
import 'Model/SkillDetailModel.dart';
import 'Url.dart';
import 'package:http/http.dart' as http;

import 'custom_centered_widget.dart';

class Setting extends StatefulWidget {
  @override
//  List<AllowCampaign> listAllowCampaign = new List();
 // List dialCodeList = new List();
  List<ListDetailModel> listDetailModel = new List();
  final bool isFromLogin ;
  List<SkillDetailModel> skillDetailModelList = [];
  List<dynamic> skillsData = new List();
  Setting({this.listDetailModel,this.skillsData,this.skillDetailModelList,this.isFromLogin= false});
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> with TickerProviderStateMixin{
  bool isShowInitBar = true;
  static const platform = const MethodChannel('MyNativeChannel');
  //List dialCodeList = new List();
  List<dynamic> skillsDataList = new List();
  List<ListDetailModel> listDetailModel = new List();
  String serverPhoneNo;
  List<String> listDetailsItems = new List();
  String ring;
  Url url = Url();
  SharedPreferences prefs;
  bool isSetCampDetails ;
  String ipAddress ;
  String accessToken ;
  String selectedDialCode ;
  String selectedListValue;
  int selectedListId;
  String selectedSkillValue ;
  int campId;
  String campName ;
  bool isShowBackBtn = false;
  List<SkillDetailModel> skillDetailModelList = [];
  List<String> skillDetailList = [];
  bool isSetStatus = false ;


  @override
  void initState() {
    getData();
//     Timer(Duration(milliseconds: 500),(){
//   //    getData();
//     });
    // TODO: implement initState
    super.initState();
  }

  Future<void> getData() async {
    setState(() {

    });
    prefs = await SharedPreferences.getInstance();
          ipAddress = prefs.getString("ipAddress") ?? '';
      accessToken = prefs.getString("accessToken") ?? '';
      campId = prefs.getInt("campId") ?? 0;
      campName = prefs.getString("campName") ?? '';
    getCampData();
    serverPhoneNo = prefs.getString("ServerPhoneNumber") ?? "";
    ring = prefs.getString("ring") ?? "";
    accessToken = prefs.getString("accessToken") ?? "";
    isShowBackBtn = prefs.getBool("showBackBtn") ?? false;
    isSetCampDetails = prefs.getBool('setCampDetails') ?? false;

    if(!isSetCampDetails) {
    }
    setState(() {});
  }



  @override
  Widget build(BuildContext context) {
    return

      Scaffold(
        appBar:PreferredSize(
          preferredSize: Size.fromHeight(95),
          child: Container(
            height: 95,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  stops: [0.4, 0.8, 1],
                  colors: [
                    // Colors are easy thanks to Flutter's Colors class.
                    Color(0xff00476d),
                    Color(0xff007981),
                    Color(0xff00a969),
                  ],
                ),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30))),
            child: Container(
              margin: EdgeInsets.only(top: 35,left: 10),
              child:  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Visibility(
                        visible: widget.isFromLogin == null || widget.isFromLogin == false ? true : false,
                        child: Container(
                          height: 40,
                          alignment: Alignment.center,
                          child: Center(
                            child: IconButton(
                              icon: Icon(
                                Icons.arrow_back_ios,
                                size: 18,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                      ),
                      widget.isFromLogin == null || widget.isFromLogin == false ? SizedBox() :
                      SizedBox(width: 15,),
                      Container(
                        alignment: Alignment.center,
                        height: 40,
                        child: Text(
                          "Settings" ?? '',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontFamily: 'roboto-regular'),
                        ),
                      ),
                    ],
                  ),

            ),
          ),
        ),
//          backgroundColor: Color(0xffe9eaed),
          backgroundColor: Colors.white,
          body: UIData()
      );
  }
  Widget CustomDropdownItem(String title,List dropdownListItem,String type) {
    debugPrint("DropdownItem $dropdownListItem");
    bool isAbsorb = false;
    if(dropdownListItem.length<=0) {
      dropdownListItem = [""];
      isAbsorb = true;
    }
    String itemValue ;
    if(type =="DIAL") {
      itemValue = selectedDialCode;
    }
    else if(type == "LIST") {
      itemValue = selectedListValue ;
    }
    else if(type=="SKILL") {
      itemValue = selectedSkillValue ;
    }
    return Container(
      margin: EdgeInsets.all(2.0),
      height: 90,
      width: MediaQuery.of(context).size.width,
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(top:12,left: 20),
                child: Text(title,style: TextStyle(fontSize:14,fontFamily: 'roboto-regular',color: Color(0xff00476d)),)),
            SizedBox(
              height: 4,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Container(
                padding: EdgeInsets.only(left: 12,right: 12),
                child: AbsorbPointer(
                  absorbing: isAbsorb,
                  child: DropdownButton(
                    underline: Container(
                      height: 1,
                      color: Color(0xffa4a9aa),
                    ),
                    icon: Image.asset("assets/2.0x/drop_down.png",width: 15,height: 15,),
                    isExpanded: true,
                    value: itemValue,
                    onChanged: (newValue){
                      setState(() {
                        if(type=="DIAL") {
                          selectedDialCode = newValue;
                        }
                        else if(type == "LIST") {
                          selectedListValue = newValue;
                        }
                        else if(type =="SKILL") {
                          selectedSkillValue = newValue;
                        }
                      }
                      );
                    },
                    items: dropdownListItem.map((item){
                      return DropdownMenuItem(
                        value: item.toString(),
                        child: Text(item,style: TextStyle(fontSize: 15,fontFamily: 'roboto-regular'),),
                      );
                    }).toList(),
                  ),
                ),
              )
            )
          ],
        ),
      ),
    );
  }

  Widget UIData() {
    return Center(
      child: Align(
        alignment: Alignment.center,
        child: isShowInitBar == false ?
        CenteredWidget(
          child: Center(
            child: Align(
              alignment: Alignment.center,
              child: ListView(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 6,left: 6,right: 6,bottom:40),
                    decoration: BoxDecoration(
                        color: Colors.white70
                    ),
                    child: Column(
                      children: <Widget>[
                        CustomDropdownItem("List",listDetailsItems,"LIST"),
                        CustomDropdownItem("Skill",skillDetailList,"SKILL"),
                        Container(
                          margin: EdgeInsets.only(top: 40,left: 20,right: 20),
                          height: 50,
                          width: MediaQuery.of(context).size.width,
                          child: RaisedButton(
                            color: Theme.of(context).buttonColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0),
                            ),
                            onPressed: (){
                              // submitWebService();
                              submit();
                            },
                            child: Text(
                              "SUBMIT",
                              style: TextStyle(color: Colors.white,fontFamily: 'roboto-regular',fontSize: 16),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        )
            :
        Center(
            child: Container(
                width: 30,
                height: 30,
                child: CircularProgressIndicator(
                ))
        ),
      ),
    ) ;
  }

  submit() {
    try {
        if(selectedListValue==null || selectedListValue.isEmpty){
        Fluttertoast.showToast(msg: "Select List value.",textColor: Colors.white,
            backgroundColor: Colors.red
        );
      }
      else if(selectedListValue ==null || selectedSkillValue==null) {
        Fluttertoast.showToast(msg: "Select Skill value.",textColor: Colors.white,
            backgroundColor: Colors.red
        );
      }
      else {
        int listId ,skillId;
        debugPrint("SUBMITSETTING");
        for(int i = 0 ; i< listDetailModel.length;i++) {
          debugPrint("SKSK $selectedListValue , ${listDetailModel[i].name}");
          if(selectedListValue==listDetailModel[i].name) {
            listId = listDetailModel[i].id;
            break;
          }
        }
        for(int i = 0 ;i<skillDetailModelList.length;i++) {
          if(selectedSkillValue == skillDetailModelList[i].name) {
            skillId = skillDetailModelList[i].id;
            break;
          }
        }

        debugPrint("LISTID $listId , SKILLID $skillId ");

        
        prefs.setBool("setCampDetails", true);
      //  prefs.setString("dialCode", selectedDialCode);
        prefs.setInt("selectedListId",listId);
        prefs.setInt("selectedSkillId",skillId);
        prefs.setString("listValue", selectedListValue);
        debugPrint("selected listid,dialcode, $selectedListId , $selectedDialCode" );
        prefs.setString("skillName", selectedSkillValue);
        prefs.setString("campName", campName);
        prefs.setInt("campId", campId);

        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
          maintainState: false,
            builder: (context) => HomePage()
        ), (Route<dynamic>route)=>false);
//        Future.delayed(Duration(milliseconds: 100),(){
//       //   Navigator.pop(context);
//          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
//              builder: (context) => HomePage()
//          ), (Route<dynamic>route)=>false);
//        });

      }
    }
    catch(e) {
      isShowInitBar = false;
      setState(() {

      });
      debugPrint("Exception : ${e.toString()} after submit at Settings Page");
    }
  }

  Future<void> getCampData() async {
    var connectivityResult = await (new Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      HttpOverrides.global = new MyHttpOverrides();
      try {

    var body = jsonEncode(  {
      "type": "APP-REQUEST",
      "data": {
        "camp_id": campId
      }
    });
    isShowInitBar = true;
    debugPrint("GETCMURL ${ipAddress + url.changeCamp}, accessToken $accessToken");
    await http.post(ipAddress + url.changeCamp,body: body, headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "Authorization": accessToken
        }).then((response) async
    {
          String responseBody = response.body;
          debugPrint("RESPONSE (GET-CAMP-DATA) $responseBody");
          var responseJSON = json.decode(responseBody);
          if (response.statusCode == 200) {
            if (responseJSON["response"] == "SUCCESS") {

            //  selectedDialCode = prefs.getString("dialCode") ?? "";
              selectedListValue = prefs.getString("listValue") ?? null;
              selectedSkillValue = prefs.getString("skillName") ?? null;
              var dataJson = responseJSON["data"];
              debugPrint("getCampData in setting ->  $dataJson , selectedListVal $selectedListValue , SlSkillVal $selectedSkillValue");
              int campId = dataJson["camp_id"];
              List skills = dataJson["skill_details"];
              for(int i=0 ;i<skills.length ; i++ ) {
                skillDetailModelList.add(
                    SkillDetailModel(
                        id: skills[i]["id"], name: skills[i]["name"]));
              }
              debugPrint("HI1");
              for(int i = 0 ;i<skillDetailModelList.length;i++) {
                skillDetailList.add(skillDetailModelList[i].name);
              }
            //  dialCodeList = (dataJson["dial_code"]);
              List listDetails = dataJson["list_details"];
              debugPrint("HI3");
              for (int i = 0; i < listDetails.length; i++) {
                listDetailModel.add(
                    ListDetailModel(
                        id: listDetails[i]["id"], name: listDetails[i]["name"]));
              }
              listDetailModel = listDetailModel;
              for(int i = 0; i< listDetailModel.length ;i++) {
                listDetailsItems.add(listDetailModel[i].name);
              }
              debugPrint("HI1");

              isShowInitBar = false;
              setState(() {

              });
              setState(() {

              });
            }
            else if(responseJSON["response"] == "FAILURE"&&responseJSON["error"]["message"]=="User active from web"){
              Url.toastShow(responseJSON["error"]["message"]);
              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                  builder: (context)=>HomePage()
              ), (route) => false);
            }
            else {
              isShowInitBar = false;
              setState(() {

              });
              Url.toastShow(responseJSON["error"]["message"], Colors.red);
              return ;
            }
            // }
          }
          else if (responseJSON["response"] == "FAILURE" &&
              (responseJSON["error"]["message"] == "Authorization Failure")) {
            prefs.setBool('loginStatus', false);
            // prefs.remove("dialCode");
            prefs.remove("listid");
            prefs.remove("_allowCamp");
            prefs.remove("selectedData");
            Url.toastShow(responseJSON["error"]["message"], Colors.red);
            goToIntroPage();
          }
          else {
            isShowInitBar = false;
            setState(() {});
            Url.toastShow(responseJSON["error"]["message"], Colors.red);
            return;
            //prefs.setBool('login_status', false);
          }
        }).timeout(Duration(seconds: 10));
      } on Exception catch (error) {
        isShowInitBar = false;
       if(this.mounted){{
         setState(() {

         });
       }}
        Url.toastShow(
            "Something went wrong. try again later. \n Error Code : $error",
            Colors.orange);
      }
    }
    else {
      Navigator.pop(context);
      Url.toastShow("Kindly check your internet connection!!");
    }
  }

  void goToIntroPage() {
    prefs.setBool("setCampDetails", false);
    prefs.setBool("loginStatus", false);
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => Login()),
            (Route<dynamic> route) => false);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  Future<void> initiateCall(String number) async {
    prefs.setBool("initiateCall4Status", true);
    String message;
    try {
      Future.delayed(Duration(milliseconds: 100)).then((val) async {
        await platform.invokeMethod('getData', {"text": number});
      });
    } on PlatformException catch (e) {
      setState(() {});
      message = "Failed to get data from native : '${e.message}'.";
      //  Url.toastShow(message);
      debugPrint("Error (during call) -> ${message}");
    }
  }

}

