
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';

class MqttService{

  final String hostName ;
  final StreamController streamController = StreamController.broadcast();
  MqttServerClient client;

  MqttService({this.hostName});



  Future<void>startService(String topic)async{
    client = MqttServerClient(this.hostName, '');
    if(client.connectionStatus.state == MqttConnectionState.connected){
      return;
      // client?.disconnect();
    }
    try{

      client.logging(on: false);
      client.keepAlivePeriod = 30;
      client.onDisconnected = onDisconnected;
      final connMess = MqttConnectMessage()
          .withClientIdentifier('Mqtt_MyClientUniqueIdWildcard')
          .keepAliveFor(30) // Must agree with the keep alive set above or not set
          .withWillTopic('willtopic') // If you set this you must set a will message
          .withWillMessage('My Will message')
          .startClean() // Non persistent session for testing
          .withWillQos(MqttQos.atLeastOnce);
      print('EXAMPLE::Mosquitto client connecting....');
      client.connectionMessage = connMess;

      try {
        await client.connect();
      } on Exception catch (e) {
        print('EXAMPLE::client exception - $e');
        mqttDisconnect();
      }

      /// Check we are connected
      if(client.connectionStatus.state == MqttConnectionState.connected) {
        print('EXAMPLE::Mosquitto client connected : TOPIC $topic');
      }
      else {
        print(
            'EXAMPLE::ERROR Mosquitto client connection failed - disconnecting, state is ${client.connectionStatus.state}');
        mqttDisconnect();
      }

      /// Ok, lets try a subscription or two, note these may change/cease to exist on the broker
      // const topic = 'sonu@java'; // Wildcard topic
      client.subscribe(topic, MqttQos.atMostOnce);


      /// The client has a change notifier object(see the Observable class) which we then listen to to get
      /// notifications of published updates to each subscribed topic.
      // ignore: avoid_types_on_closure_parameters

      debugPrint("INIT-MQTT");
      client.updates.listen((List<MqttReceivedMessage<MqttMessage>> c) {
        final MqttPublishMessage recMess = c[0].payload;
        final pt =
        MqttPublishPayload.bytesToStringAsString(recMess.payload.message);

        /// The above may seem a little convoluted for users only interested in the
        /// payload, some users however may be interested in the received publish message,
        /// lets not constrain ourselves yet until the package has been in the wild
        /// for a while.
        /// The payload is a byte buffer, this will be specific to the topic
        streamController.add(pt);
        print(
            'EXAMPLE::Change notification:: topic is <${c[0].topic}>, payload is <-- $pt -->');
        print('');
        //getCrmData();
      });

      /// Ok, we will now sleep a while, in this gap you will see ping request/response
      /// messages being exchanged by the keep alive mechanism.
      print('EXAMPLE::Sleeping....');

      /// Finally, unsubscribe and exit gracefully

    }
    catch(e) {
      print('EXAMPLE::Unsubscribing');
      if(client!=null){
        client.unsubscribe(topic);
      }

      /// Wait for the unsubscribe message from the broker if you wish.
      await MqttUtilities.asyncSleep(2);
      print('EXAMPLE::Disconnecting');
      mqttDisconnect();
      Fluttertoast.showToast(msg: "mqtt Disconnected");
      debugPrint("Exception : (mqtt) $e");
    }
  }


  /// The unsolicited disconnect callback
  void onDisconnected() {
    if(streamController!=null) streamController?.close();
    print('EXAMPLE::OnDisconnected client callback - Client disconnection');
  }

  mqttDisconnect() {
    if(streamController!=null) streamController?.close();
    if(client!=null) client.disconnect();
  }
}
