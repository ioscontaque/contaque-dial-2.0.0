import 'package:sqflite/sqflite.dart';

final String tableName = 'pausecodeTable';
final String columnId = '_id';
final String col1 = 'name';
final String col2 = 'details';

class SqlHelper {
  int id;
  String name;
  String details;

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      col1: name,
      col2: details
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }

  SqlHelper();

  SqlHelper.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    name = map[col1];
    details = map[col2];
  }
}

class SqlProvider {
  Database db;

  Future open(String path) async {
    db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
          await db.execute('''
create table $tableName ( 
  $columnId integer primary key, 
  $col1 text not null,
  $col2 text not null)
''');
        });
  }

  Future<SqlHelper> insert(SqlHelper helper) async {
    helper.id = await db.insert(tableName, helper.toMap());
    return helper;
  }

  Future<SqlHelper> getTodo(int id) async {
    List<Map> maps = await db.query(tableName,
        columns: [columnId, col1, col2],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return SqlHelper.fromMap(maps.first);
    }
    return null;
  }
}