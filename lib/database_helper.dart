import 'dart:async';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'HomePage/ContactModel.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;
  static Database _db;
  int version = 1;

  Future<Database> get db async {
    if (_db != null && _db.isOpen) return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "contaque.db");
    var theDb = await openDatabase(path, version: version, onCreate: _onCreate);

    return theDb;
  }

  void _onCreate(Database db, int version) async {
    // When creating the db, create the table
    await db.execute(
        "CREATE TABLE IF NOT EXISTS User(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, fullname TEXT, mobile_no TEXT)");
  }

  Future<List<UserContact>> getAllUser() async {
    var dbClient = await db;
    List<Map> list =
        await dbClient.rawQuery('SELECT * FROM User order by LOWER(fullname)');
    List<UserContact> userList = new List();
    for (int i = 0; i < list.length; i++) {
      var user = new UserContact(list[i]["fullname"], list[i]["mobile_no"]);
      user.setUserId(list[i]["id"]);
      userList.add(user);
    }
    return userList;
  }
}
