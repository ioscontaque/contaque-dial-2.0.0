import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
class Url {
  String login = "/api/login";
  String logOut = "/api/logout";
  String getUserDetail = "/api/get-user-data";  // inbound , both
  String setDefaultData = "/api/set-campaign-data";
  String changePassword = "/api/change-password"; // removed
  String dialCall = "/api/manual-dial";
  String callLogs = "/api/call-logs";
  String dispositionUrl = "/api/disposition-request";
  String checkCallStatus = "/api/get-user-status";  // removed
  String changeCamp = "/api/get-campaign-data";
  String forceLogin =  "/api/force-login";
  String getCRMData =  "/api/get-crm-data";
  String setUSerStatus = "/api/set-user-status";
  String crm = "/api/crm";

  static void toastShow(String message,[Color color=Colors.red]){
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: color,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
