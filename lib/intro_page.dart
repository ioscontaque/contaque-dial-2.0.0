import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:connectivity/connectivity.dart';
import 'package:dialer/HomePage/HomePage.dart';
import 'package:dialer/Url.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'HomePage/CRMPage.dart';
import 'Login/Login.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart' as http;

import 'Setting.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {

  SwiperController _swiperController = SwiperController();
  bool loader = false;
  int _currentIndex = 0;
  List<String> images = ["assets/2.0x/Screen1.png","assets/2.0x/menu_bg1.png","assets/2.0x/menu_bg2.png"];
  List<String> titles = ["Easy to provide 24x7 support",
                          "Create inbound and outbound call", "Secure connection between customer and Agent"];

  final List<Color> colors = [
    Colors.green.shade300,
    Colors.blue.shade300,
    Colors.indigo.shade300
  ];

  SharedPreferences prefs;
  bool isSetCampDetails  = true;
  bool isCheckCallStatus = true;
  Url url = Url();
  String ipAddress;
  String accessToken;
  bool isShowProgressBar = true;

  init() async {
    prefs = await SharedPreferences.getInstance();
    bool loginStatus = prefs.getBool("loginStatus") ?? false;
    debugPrint("LoginStatus $loginStatus");
    if(loginStatus == true) {
      Future.delayed(Duration(seconds: 1),(){
        userStatusService();
      });
    }
    else {
      isShowProgressBar = false;
      setState(() {

      });
    }
  }

  gotoHome(){
    Navigator.pushReplacement(context, MaterialPageRoute(
      builder: (context) => HomePage()
    ));
  }


  Future<void> userStatusService() async {
    setState(() {

    });
    var connectivityResult = await (new Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      HttpOverrides.global = new MyHttpOverrides();

      ipAddress = prefs.getString("ipAddress") ?? "";
      accessToken = prefs.getString("accessToken") ?? "";
      debugPrint(
          "USERSTATUSSERVICE URL ${ipAddress } , ${url.checkCallStatus}   , AccToken ${accessToken}");
      debugPrint("calling:: checkcallstatus");
      try {
        await http.post(ipAddress + url.checkCallStatus, headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "Authorization": accessToken
        }).then((response) async {
          String responseBody = response.body;
          debugPrint("Res Body : $responseBody");
          var responseJSON = json.decode(responseBody);
          debugPrint("checkCallStatusCode ${response.statusCode}");
          if (response.statusCode == 200) {
            if (responseJSON["response"] == "SUCCESS") {
              prefs.setBool("open_crm", false);
              isCheckCallStatus = false; // for inbound call ..
              var arrData = responseJSON["data"];
              debugPrint("DDHOMe $arrData");
              var list = arrData;
              Map myMap = list;
              debugPrint("USerSTATUSSERVICE MAPDATA ${myMap}");
                if(myMap["status"]=="ACW" || myMap["status"]=="INCALL"||myMap["status"]=="DIAL"
                ||myMap["status"] =="FOLLOWUP" || myMap["status"] =="QUEUE"
                ){
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => CRMPage()));
              }
              else  {
                prefs.setBool("openCrm", false);
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => HomePage()),
                        );

              }
            }
            else if(responseJSON["response"] == "FAILURE"&&responseJSON["error"]["message"]=="User active from web"){
              Url.toastShow(responseJSON["error"]["message"]);
              Navigator.pushReplacement(context, MaterialPageRoute(
                  builder: (context)=>HomePage()
              ));
            }
            else {

            }
          }
          else if (responseJSON["response"] == "FAILURE" &&
              (responseJSON["error"]["message"] == "Authorization Failure")) {
            prefs.setBool('loginStatus', false);
            // prefs.remove("dialCode");
            prefs.remove("listid");
            prefs.remove("_allowCamp");
            prefs.remove("selectedData");
            Url.toastShow(responseJSON["error"]["message"], Colors.red);
            prefs.setBool("loginStatus", false);
            isShowProgressBar = false;
            setState(() {

            });
          }
          else if (response.statusCode == 401) {
            prefs.setBool("loginStatus", false);
            isShowProgressBar = false;
            setState(() {

            });
//            setState(() {
//
//            });
//            Navigator.push(
//              context,
//              MaterialPageRoute(builder: (context) => Login()), // Login
//            );
          }
          else {
            isShowProgressBar = false;
            setState(() {

            });
            Url.toastShow(responseJSON["message"], Colors.red);
          }
        }).timeout(Duration(seconds: 15));
      } on Exception catch (error) {
        isShowProgressBar = false;
        setState(() {

        });
        // When Service is not Working
        debugPrint("Error : $error");
//        Url.toastShow(
//            "Something went wrong. try again later. \n Error Code : $error",
//            Colors.orange);
      }
    } else {
      Url.toastShow("Kindly check your internet connection!!");
    }
  }

  @override
  void initState() {
    setState(() {

    });
    init();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isShowProgressBar == true ?
      Center(
        child: Container(
          height: 30,
          width: 30,
          child: CircularProgressIndicator(),
        ),
      ) :
      Center(
        child: Container(
          height : MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              // image:_currentIndex == 0 ? null :  DecorationImage(
              //     fit: BoxFit.cover,
              //     image:_currentIndex ==0 ? AssetImage("assets/2.0x/Screen1.png") : AssetImage("assets/2.0x/background.png")
              // )
          ),
          child: Stack(
            children: <Widget>[
              IntroItem(),
              Align(
                  alignment: Alignment.bottomLeft,
                  child: GestureDetector(
                    onTap: () {
                      loader = true;
                      _onLoading();
                      Timer(Duration(seconds: 2), (){
                        goToLogin();
                      });
                    },
                    child: Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                        ),
                        margin: EdgeInsets.all(20),
                        child: Center(
                            child: Text("Skip",style: TextStyle(fontSize: 18,color: Colors.white),textAlign: TextAlign.center,
                            ))
                    ),

                  )
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  height: 50,
                  width: 50,
                  margin:EdgeInsets.all(20.0),
                  child: IconButton(
                      onPressed: (){
                        if(_currentIndex < 2) {
                          _swiperController.next(animation: true);
                        } else {
                          loader = true;
                          _onLoading();
                          Timer(Duration(seconds: 2), (){
                            goToLogin();
                          });
                        }
                      },
                      icon: Icon(_currentIndex < 2 ? Icons.navigate_next : Icons.check,color: Colors.white,size: 28,)),
                ),

              )
            ],
          ),
        ),
      ),
    ) ;
  }

  Widget IntroItem() {
     return Container(
       height: MediaQuery.of(context).size.height,
       decoration: BoxDecoration(
           image:  DecorationImage(
               fit: BoxFit.cover,
               image: AssetImage("assets/2.0x/background.png")
           )
       ),
       child: Swiper(
         controller: _swiperController,
           loop :false,
         physics: ScrollPhysics(),
         itemCount: 3,
         index: _currentIndex,
         onIndexChanged: (index) {
             setState(() {
               _currentIndex = index;
             });
         },
         pagination: SwiperPagination(
           builder: DotSwiperPaginationBuilder(
             activeColor: Color(0xff007981),
             activeSize: 20.0
           )
         ),
         itemBuilder: (context,index) {
           return Stack(
             children: <Widget>[
               Container(
                   height:  MediaQuery.of(context).size.height ,
                   padding: EdgeInsets.symmetric(horizontal: 30),
                   decoration: BoxDecoration(
                       image:
                       // index == 0?
                       DecorationImage(
                           fit: BoxFit.cover,
                           image: AssetImage(images[index])
                       )
                       //     : DecorationImage(
                       //     fit: BoxFit.fill,
                       //     image:  AssetImage("assets/2.0x/background.png")
                       // ) ,
                      // borderRadius:index == 0? BorderRadius.circular(0): BorderRadius.circular(20)
                   ),
                   width: double.infinity,
                   child:Center(
                     child: Container(
                       width: 300,
                       height: 300,
                       constraints: BoxConstraints(
                           maxHeight: 300
                       ),
                       child:
                       //index == 0 ?
                       Text("")
               //:
                       // SvgPicture.asset(images[index],width: 200,height: 200,fit: BoxFit.contain,
                       //   placeholderBuilder: (context){
                       //     return Center(
                       //         child: Container(
                       //             width: 30,
                       //             height: 30,
                       //             child: CircularProgressIndicator(
                       //             ))
                       //     );
                       //   },
                       // ),
                     ),
                   )

               ),
               Visibility(
                 visible:
                 //index == 0 ?
                 false ,
                     //:true,
                 child: Container(
                   width: double.infinity,
                   margin: EdgeInsets.only(top: 150),
                   padding: EdgeInsets.symmetric(horizontal: 20),
                   child: Text(titles[index],textAlign: TextAlign.center,
                   style: TextStyle(color: Colors.white,fontSize: 22),
                   ) ,
                 ),
               ),

             ],
           );
         },
       ),
     );
  }
  void _onLoading() {
    if (loader == true) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => new Center(
          child: CircularProgressIndicator(
            valueColor:
            new AlwaysStoppedAnimation<Color>(new Color(0xFF4B8BF4)), // new Color(0xFF4B8BF4)
          ),
        ),
      );
    } else {
      Navigator.pop(context);
    }
  }
  goToLogin() {
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>Login()),
            (Route<dynamic> route) => false
    );
  }

}
