import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget {
  String title;
  bool isShowIcon = true;
  CustomAppBar({@required this.title,this.isShowIcon = true});

  @override
  Widget build(BuildContext context) {
    return  Container(
      height: 95,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            stops: [0.4, 0.8, 1],
            colors: [
              // Colors are easy thanks to Flutter's Colors class.
              Color(0xff00476d),
              Color(0xff007981),
              Color(0xff00a969),
            ],
          ),
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30))),
      child: Container(
        margin: EdgeInsets.only(top: 35,left: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            isShowIcon==true ?
            Container(
              height: 40,
              alignment: Alignment.center,
              child: Center(
                child: IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    size: 18,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ) : SizedBox(width: 30,),
            Container(
              alignment: Alignment.center,
              height: 40,
              child: Text(
                title ?? '',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontFamily: 'roboto-regular'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
