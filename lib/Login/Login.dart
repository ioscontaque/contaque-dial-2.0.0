import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dialer/Model/AllowCampaign.dart';
import 'package:dialer/Model/ListDetailModel.dart';
import 'package:dialer/Model/SkillDetailModel.dart';
import 'package:dialer/sqlhelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dialer/Setting.dart';
import 'package:dialer/Url.dart';
import 'package:connectivity/connectivity.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';
import 'package:imei_plugin/imei_plugin.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';

class Login extends StatefulWidget {
  const Login({Key key}) : super(key: key);

  @override
  LoginState createState() => new LoginState();
}

class LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  final _userNameErrorKey = GlobalKey();
  final _passErrorKey = GlobalKey();
  final _ipAddressErrorKey = GlobalKey();
  final _portErrorKey = GlobalKey();
  final txtUserId = new TextEditingController();
  final txtPass = new TextEditingController();
  final txtIpAddress = new TextEditingController();
  Url url = Url();
  bool loader = false;
  String urlValue;
  static const platform = const MethodChannel('MyNativeChannel');
  SharedPreferences prefs;

  String msg = "No Message";
  bool status = false;
  String imei = "";
  String ip;
  DateTime date;
  String userTextHint = "Username*";

  Tooltip userErrorIcon;
  Tooltip passwordErrorIcon;
  Tooltip ipAddressErrorIcon;
  Tooltip portErrorIcon;

  static const Color inputTextColor = Color(0xffd2d3d1);
  static const Color prefixIconColor = Color(0xff828580);
  static const Color lineColor = Color(0xffa4a9aa);
  Database database ;


  @override
  void initState() {
    _askPermission();
    getData();
    removeFields();
    super.initState();
  }

  removeFields()async {
    prefs = await SharedPreferences.getInstance();
    prefs.remove("selectedListId");
    prefs.remove("selectedSkillId");
    prefs.remove("listValue");
    prefs.remove("skillName");
  }

  // ** Ask permissions using in build method using permission_handler lib **
  Future<void> _askPermission() async {
    try {
      Timer(Duration(milliseconds: 200), () async {
        prefs = await SharedPreferences.getInstance();
        PermissionStatus contactStatus = await PermissionHandler()
            .checkPermissionStatus(PermissionGroup.contacts);
        if (contactStatus != PermissionStatus.granted) {
          Map<PermissionGroup, PermissionStatus> permissions =
          await PermissionHandler().requestPermissions(
              [PermissionGroup.contacts, PermissionGroup.phone]);
        }
      });
    } catch (e) {
      debugPrint("Exception : $e");
    }
  }

  // ** get stored data from SharedPreferences **
  Future<void> getData() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      prefs.setBool("loginStatus", false);
      if (this.mounted) {
        setState(() {
          txtIpAddress.text = prefs.getString("ipAddress") ?? '';
        });
      }
    } catch (e) {
      Url.toastShow("Error : ${e.toString()}");
    }
  }

  @override
  void dispose()  async{
    try {
      txtUserId.dispose();
      txtPass.dispose();
      txtIpAddress.dispose();
    } catch (e) {}
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery
        .of(context)
        .size
        .height;
    double _width = MediaQuery
        .of(context)
        .size
        .width;
    return new WillPopScope(
      onWillPop: () {
        return Future.value(true);
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            decoration: BoxDecoration(color: Colors.white),
            child: Stack(
              children: <Widget>[
                Positioned(
                  bottom: 8,
                  child: Container(
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    child: Text(
                      "Version 3.0.0",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Color(0xff00476d)),
                    ),
                  ),
                ),
                Form(
                  key: _formKey,
                  child: Container(
                    width: _width,
                    height: _height,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Container(
                            height: 80,
                            constraints: BoxConstraints(maxWidth: 280),
                            child: Image.asset("assets/2.0x/logo.png"),
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          Container(
                            constraints: BoxConstraints(maxWidth: 300),
                            height: 45,
                            child: new TextFormField(
                              onChanged: (value) {
                                if (value.isNotEmpty) {
                                  if (userErrorIcon != null) {
                                    userErrorIcon = null;
                                    setState(() {

                                    });
                                  }
                                }
                              },
                              controller: txtUserId,
                              autocorrect: false,
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(50),
                              ],
                              style: const TextStyle(
                                color: Colors.black,
                                fontFamily: "arial",
                              ),
                              decoration: new InputDecoration(
                                suffixIcon: userErrorIcon,
                                icon: Image.asset('assets/2.0x/user-icon.png',
                                    color: prefixIconColor,
                                    height: 20,
                                    width: 20),
                                border: InputBorder.none,
                                hintText: userTextHint,
                                hintStyle: const TextStyle(
                                    color: inputTextColor,
                                    fontFamily: "roboto-regular"),
                              ),
                            ),
                          ),
                          Container(
                            height: 1,
                            constraints: BoxConstraints(maxWidth: 300),
                            color: lineColor,
                          ),
                          Container(
                            constraints: BoxConstraints(maxWidth: 300),
                            height: 45,
                            child: new TextFormField(
                              onChanged: (value) {
                                if (value.isNotEmpty) {
                                  if (passwordErrorIcon != null) {
                                    passwordErrorIcon = null;
                                    setState(() {

                                    });
                                  }
                                }
                              },
                              obscureText: true,
                              controller: txtPass,
                              autocorrect: false,
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(200),
                              ],
                              style: const TextStyle(
                                color: Colors.black,
                                fontFamily: "roboto-regular",
                              ),
                              decoration: new InputDecoration(
                                suffixIcon: passwordErrorIcon,
                                icon: Image.asset('assets/2.0x/lock.png',
                                    color: prefixIconColor,
                                    height: 20,
                                    width: 20),
                                border: InputBorder.none,
                                hintText: "Password*",
                                hintStyle: const TextStyle(
                                    color: inputTextColor,
                                    fontFamily: "roboto-regular"),
                              ),
                            ),
                          ),
                          Container(
                            height: 1,
                            constraints: BoxConstraints(maxWidth: 300),
                            color: lineColor,
                          ),
                          Container(
                            constraints: BoxConstraints(maxWidth: 300),
                            height: 45,
                            child: new TextFormField(
                              onChanged: (value) {
                                if (value.isNotEmpty) {
                                  if (ipAddressErrorIcon != null) {
                                    ipAddressErrorIcon = null;
                                    setState(() {

                                    });
                                  }
                                }
                              },
                              controller: txtIpAddress,
                              autocorrect: false,
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(200),
                              ],
                              style: const TextStyle(
                                color: Colors.black,
                                fontFamily: "roboto-regular",
                              ),
                              decoration: new InputDecoration(
                                suffixIcon: ipAddressErrorIcon,
                                icon: Image.asset('assets/2.0x/location.png',
                                    color: prefixIconColor,
                                    height: 20,
                                    width: 20),
                                border: InputBorder.none,
                                hintText: "IP Address/Domain Name*",
                                hintStyle: const TextStyle(
                                    color: inputTextColor, fontFamily: "arial"),
                              ),
                            ),
                          ),
                          Container(
                            height: 1,
                            constraints: BoxConstraints(maxWidth: 300),
                            color: lineColor,
                          ),
                          Container(
                            height: 1,
                            constraints: BoxConstraints(maxWidth: 300),
                            color: lineColor,
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          Container(
                            width: 300,
                            height: 50,
                            constraints: BoxConstraints(maxWidth: 360),
                            child: RaisedButton(
                              color: Theme
                                  .of(context)
                                  .buttonColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30)),
                              child: Text("LOGIN",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: 'roboto-regular',
                                    color: Colors.white,
                                  )),
                              onPressed: () {
                                FocusScope.of(context).unfocus();
                                loginService();
                              },
                            ),
                          ),
                        ]),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _onLoading() {
    if (loader == true) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) =>
        new Center(
          child: CircularProgressIndicator(
              valueColor:
              new AlwaysStoppedAnimation<Color>(new Color(0xFF4B8BF4))
            //  new AlwaysStoppedAnimation<Color>(new Color(0xFF4B8BF4)),
          ),
        ),
      );
    } else {
      if (this.mounted) {
       if(Navigator.canPop(context)) {
         Navigator.pop(context);
       }
      }
    }
  }

  // ** login Action Operation **
  loginService() async {
    imei = await ImeiPlugin.getImei(
        shouldShowRequestPermissionRationale: false); // get IMEI No of device
    if (imei == null) {
      imei = await ImeiPlugin.getId();
    }

    final prefs = await SharedPreferences.getInstance();
    prefs.setBool("showBackBtn", false);
    prefs.setBool('autoRefreshContact', true);
    var userId = txtUserId.text.trim();
    var pass = txtPass.text.trim();
    bool isBlank = false;
    if (txtUserId.text.isEmpty || userId == "") {
      userErrorIcon =
          errorIcon(title: "Enter your username", errorKey: _userNameErrorKey);
      isBlank = true;
    }
    if (txtPass.text.isEmpty || pass == "") {
      passwordErrorIcon =
          errorIcon(title: "Enter your password", errorKey: _passErrorKey);
      isBlank = true;
    }
    if (txtIpAddress.text.isEmpty || txtIpAddress.text.trim() == "") {
      ipAddressErrorIcon =
          errorIcon(title: "Enter IP Address/Domain Name", errorKey: _ipAddressErrorKey);
      isBlank = true;
    }
    if (isBlank) {
      setState(() {});
      return;
    }
     ip = txtIpAddress.text.trim();
    if(!ip.contains("http") && !ip.contains("https")) {
      ip = "https://" + ip;
    }
    var connectivityResult = await (new Connectivity().checkConnectivity());
    Uri uri = Uri.parse(ip);
    prefs.setString('ipAddress', ip);
    prefs.setString('hostName', uri.host);

    urlValue = ip+ url.login;
    debugPrint("IPAddress $ip");
    debugPrint("hostName ${uri.host}");

    if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      loader = true;
      _onLoading();
      HttpOverrides.global = new MyHttpOverrides();
      Url loginUrl = new Url();
      var body = jsonEncode({
        "type": "APP-REQUEST",
        "data": {
          "user_id": txtUserId.text.replaceAll(RegExp(" "), ""),
          "password": txtPass.text,
          "imei_no": imei
        }
      });
      /* if any exception occurs during request processing then show message
           and stay on the same page
        */
      try {
        debugPrint("urlVal-> $urlValue , body -> $body");
        /*  post request in background and get response after processing Url  */
        await http.post(urlValue, body: body, headers: {
          "content-type": "application/json"
        }).then((response)
        async {
          var responseJSON = json.decode(response.body);
          debugPrint("Login RES LOGIN ${responseJSON} , statusCode ${response.statusCode}");
          if (response.statusCode == 200) {
            String jsonResponse = response.body;
            var jsonResponseData = json.decode(jsonResponse);
            if(jsonResponseData["response"] == "SUCCESS")
              {
               debugPrint("LOGINSERVICE");
               var dataJson = jsonResponseData["data"];
               String ring =  dataJson["ring"] ?? "";
               String agentId = dataJson["agent_id"]?.toString();
               prefs.setString("ring", ring);
               prefs.setString("agentId", agentId.toString());
//               "ring": "Y/N"
                debugPrint("DATAJ ${dataJson}");
                //debugPrint("DATAJSON ${dataJson[0]["skills"]}");
                //dataJson = dataJson[0];
                int campId = dataJson["camp_id"];
                prefs.setInt("campId", campId);
                debugPrint("CAMPID");
                List skills = dataJson["skill_details"];
                List<SkillDetailModel> skillDetailModelList = [];
                for(int i=0 ;i<skills.length ; i++ ) {
                  skillDetailModelList.add(
                      SkillDetailModel(
                          id: skills[i]["id"], name: skills[i]["name"]));
                }
                List skillsData = skills;
                debugPrint("SKillsList $skillsData");
               // List dialCodeList = (dataJson["dial_code"]);
                List listDetails = dataJson["list_details"];
                List<ListDetailModel> listDetailModel = [];
                for (int i = 0; i < listDetails.length; i++) {
                  listDetailModel.add(
                      ListDetailModel(
                          id: listDetails[i]["id"], name: listDetails[i]["name"]));
                }
                String userFullName = dataJson["user_full_name"]??"";
                prefs.setString("userName", userFullName);
                String accessToken = dataJson["access_token"] ?? "";
                String dialingNumber = dataJson["dialing_number"] ?? "";
                String manualAllow = dataJson["manual_allow"] ?? "";
                prefs.setString("manualAllow",manualAllow);
                prefs.setString("skills", skills.toString());
                prefs.setString('password', txtPass.text.trim().toString());
                prefs.setString("accessToken", accessToken); //accessToken
                prefs.setString("ServerPhoneNumber",dialingNumber); // prefs.getString('password')
                prefs.setBool("setCampDetails", false);
                var pausedCodeList = dataJson["pause_codes"]??"";
                debugPrint("PAUSEDCODE $pausedCodeList");
                SqlProvider provider = SqlProvider();
                var databasesPath = await getDatabasesPath();
                await provider.open("pausedCode");
                SqlHelper helper = SqlHelper();
                try{
                  provider.db.delete("pausecodeTable");
                }
                catch(e)
          {
            Fluttertoast.showToast(msg: "Error : $e");
                }
                helper.id = 1;
                helper.name = "READY";
                helper.details = "AUTO";
                provider.insert(helper);
                if(pausedCodeList==null || pausedCodeList.length ==0){
                  helper.id = 2;
                  helper.name  = "PAUSED";
                  helper.details = "AUX";
                  provider.insert(helper);
                }
                else {
                  int id = 2;
                  for(var item in pausedCodeList) {
                    helper.id = id;
                    helper.name = item["name"];
                    helper.details = item["details"];
                    provider.insert(helper);
                    id++;
                  }
                }
                List<Map<String, dynamic>> records = await provider.db.query('pausecodeTable');
                debugPrint("QUERY C ${records.length} , ${records}");
                prefs.setBool("loginStatus", true);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            Setting(
                              isFromLogin: true,
                                listDetailModel: listDetailModel,
                                skillsData: skillsData,skillDetailModelList:skillDetailModelList)));
         }
            else {
              loader = false;
              _onLoading();
              debugPrint("Error: ${response.body}");
              Url.toastShow(
                  "Something went wrong :  ${jsonResponseData["error"]["message"]}", Colors.orange);
              return;
            }
          }
          else {
            loader = false;
            _onLoading();
            if (responseJSON["error"]["message"] == "force-login") {
              showDialog(context: context,
              builder: (BuildContext context) {
                return Center(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 30),
                    color: Colors.transparent,
                    width: 300.0,
                    height: 200.0,
                    child: Card(
                      margin: EdgeInsets.all(0),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              child: Text(
                                "Force Login",
                                style: TextStyle(
                                    fontSize: 22,
                                    color: Color(0xff00476d),
                                    decoration: TextDecoration.none,
                                    fontFamily: 'roboto-regular'),
                              ),
                            ),
                            SizedBox(height: 20),
                            Container(
                              child: Text(
                                "Would you like to force login?",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Color(0xff828580),
                                    decoration: TextDecoration.none,
                                    fontFamily: 'roboto-regular'),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Expanded(
                              child: Container(
                                height: 50,
                                margin: EdgeInsets.only(top: 10),
                                child: Container(
                                  decoration: BoxDecoration(
                                      border: Border(
                                          top: BorderSide(
                                              color: Color(0xffa4a9aa)))),
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                          padding: EdgeInsets.only(left: 4),
                                          child: Material(
                                              color: Colors.white,
                                              child: InkWell(
                                                  onTap: () {
                                                    Navigator.pop(context);
                                                    forceLogin();
                                                  },
                                                  child: Center(
                                                      child: Text("Yes",
                                                          style: TextStyle(
                                                              fontSize: 22,
                                                              color: Color(
                                                                  0xff00476d),
                                                              decoration:
                                                              TextDecoration
                                                                  .none,
                                                              fontFamily:
                                                              'roboto-regular'))))),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          padding: EdgeInsets.only(right: 4),
                                          child: Material(
                                              color: Colors.white,
                                              child: InkWell(
                                                  onTap: () {
                                                    Navigator.pop(context);
                                                  },
                                                  child: Center(
                                                      child: Text("Cancel",
                                                          style: TextStyle(
                                                              fontSize: 22,
                                                              color: Color(
                                                                  0xff828580),
                                                              decoration:
                                                              TextDecoration
                                                                  .none,
                                                              fontFamily:
                                                              'roboto-regular'))))),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),

                  ),
                );

              });
            }
            else {
                Url.toastShow(responseJSON["error"]["message"], Colors.red);
              return;
            }
          }
        }).timeout(Duration(seconds: 30)).then((value) {

        });
      }
      catch (e) {
        loader = false;
        _onLoading();
        debugPrint("EEEEE $e");
        Fluttertoast.showToast(msg: "server not reachable.",
            textColor: Colors.white,
            backgroundColor: Colors.red
        );
        return;
      }
    } else {
//        loader = false;
//        _onLoading();
      Url.toastShow("Kindly check your internet connection!!");
      return;
    }
  }

  // ** Force login if user active in another device **
  forceLogin() async {
    var userId = txtUserId.text.trim();
    var pass = txtPass.text.trim();

    if (txtUserId.text.isEmpty || userId == "") {
      Url.toastShow("Enter User Name.");
    } else if (txtPass.text.isEmpty || pass == "") {
      Url.toastShow("Enter Password!!");
    } else if (txtIpAddress.text.isEmpty || txtIpAddress.text.trim() == "") {
      Url.toastShow("Enter your IP address/Domain Name!!");
    }

    else {
        ip = "https://" + txtIpAddress.text;
      var connectivityResult = await (new Connectivity().checkConnectivity());
      loader = true;
      _onLoading();

      if (connectivityResult == ConnectivityResult.wifi ||
          connectivityResult == ConnectivityResult.mobile) {
        HttpOverrides.global = new MyHttpOverrides();

        var body = jsonEncode({
          "type": "APP-REQUEST",
          "data": {
            "force_login": true,
            "user_id": txtUserId.text.replaceAll(RegExp(" "), ""),
            "password": txtPass.text,
            "imei_no": imei
          }
        });
        try {
          debugPrint("Login URL ${urlValue}");
          await http.post(urlValue, body: body, headers: {
            "content-type": "application/json"
          }).then((response) async {
            var responseJSON = json.decode(response.body);
            debugPrint(
                "Force Login RES LOGIN ${responseJSON} , code ${response.statusCode}");
            if (response.statusCode == 200) {
              String jsonResponse = response.body;
              var jsonResponseData = json.decode(jsonResponse);
              if (jsonResponseData["response"] == "SUCCESS") {
                loader = false;
                _onLoading();
                debugPrint("Force-Login RSPONS $jsonResponse");
                var dataJson = jsonResponseData["data"];
                String ring =  dataJson["ring"] ?? "";
                String agentId = dataJson["agent_id"]?.toString();
                prefs.setString("ring", ring);
                prefs.setString("agentId", agentId.toString());
//               "ring": "Y/N"
                debugPrint("DATAJ ${dataJson}");
                //debugPrint("DATAJSON ${dataJson[0]["skills"]}");
                //dataJson = dataJson[0];
                int campId = dataJson["camp_id"];
                prefs.setInt("campId", campId);
                debugPrint("CAMPID");
                List skills = dataJson["skill_details"];
                List<SkillDetailModel> skillDetailModelList = [];
                for(int i=0 ;i<skills.length ; i++ ) {
                  skillDetailModelList.add(
                      SkillDetailModel(
                          id: skills[i]["id"], name: skills[i]["name"]));
                }
                List skillsData = skills;
                debugPrint("SKillsList $skillsData");
                List listDetails = dataJson["list_details"];
                List<ListDetailModel> listDetailModel = [];
                for (int i = 0; i < listDetails.length; i++) {
                  listDetailModel.add(
                      ListDetailModel(
                          id: listDetails[i]["id"], name: listDetails[i]["name"]));
                }
                String userFullName = dataJson["user_full_name"]??"";
                prefs.setString("userName", userFullName);
                String accessToken = dataJson["access_token"] ?? "";
                String dialingNumber = dataJson["dialing_number"] ?? "";
                String manualAllow = dataJson["manual_allow"] ?? "";
                prefs.setString("manualAllow",manualAllow);
                prefs.setString("skills", skills.toString());
                prefs.setString('password', txtPass.text.trim().toString());
                prefs.setString("accessToken", accessToken); //accessToken
                prefs.setString("ServerPhoneNumber",dialingNumber); // prefs.getString('password')
                prefs.setBool("setCampDetails", false);
                var pausedCodeList = dataJson["pause_codes"]??"";
                debugPrint("PAUSEDCODE $pausedCodeList");
                SqlProvider provider = SqlProvider();
                var databasesPath = await getDatabasesPath();
                await provider.open("pausedCode");
                SqlHelper helper = SqlHelper();
                try{
                  provider.db.delete("pausecodeTable");
                }
                catch(e) {
                  Fluttertoast.showToast(msg: "Error : $e");
                }
                helper.id = 1;
                helper.name = "READY";
                helper.details = "AUTO";
                provider.insert(helper);
                if(pausedCodeList==null || pausedCodeList.length ==0){
                  helper.id = 2;
                  helper.name  = "PAUSED";
                  helper.details = "AUX";
                  provider.insert(helper);
                }
                else {
                  int id = 2;
                  for(var item in pausedCodeList) {
                    helper.id = id;
                    helper.name = item["name"];
                    helper.details = item["details"];
                    provider.insert(helper);
                    id++;
                  }
                }
                List<Map<String, dynamic>> records = await provider.db.query('pausecodeTable');
                debugPrint("QUERY C ${records.length} , ${records}");
                prefs.setBool("loginStatus", true);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            Setting(
                              isFromLogin: true,
                                listDetailModel: listDetailModel,
                                skillsData: skillsData,skillDetailModelList:skillDetailModelList)));
              }
              else {
                debugPrint("msg ${response.body}");
                loader = false;
                _onLoading();
                Url.toastShow(
                    "Something went wrong :  ${jsonResponseData["error"]["message"]}", Colors.orange);
                return;
              }
            }
            else {
              Fluttertoast.showToast(msg: responseJSON["error"]["message"],
              backgroundColor: Colors.red,textColor: Colors.white
              );
              loader = false;
              _onLoading();
            }
          }
          ).timeout(Duration(seconds: 30));
        }
        catch (e) {
          if(loader == true) {
            loader = false;
            _onLoading();
          }
//          Fluttertoast.showToast(
//              msg: "please try again.",
//              textColor: Colors.white,
//              backgroundColor: Colors.red
//          );
        }
      } else {
        if(loader == true) {
          loader = false;
          _onLoading();
        }
        Fluttertoast.showToast(msg: "Kindly check your internet connection.",
            textColor: Colors.white,
            backgroundColor: Colors.red
        );
//        Url.toastShow("Kindly check your internet connection!!");
      }
    }
    if(loader == true) {
      loader = false;
      _onLoading();
    }
  }
}

Widget errorIcon({title, errorKey}) {
  return Tooltip(
      key: errorKey,
      message: title ?? '',
      child: IconButton(
        icon: Icon(Icons.error, color: Colors.red,),
        onPressed: () {
          final dynamic tooltip = errorKey.currentState;
          tooltip.ensureTooltipVisible();
        },
      )
  );
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    try {
      HttpClient client =
      super.createHttpClient(context); //<<--- notice 'super'
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    } catch (e) {
      Url.toastShow("Error Message : ${e.toString()}", Colors.red);
    }
    return new HttpClient();
  }
}
