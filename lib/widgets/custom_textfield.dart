import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {

  Tooltip errorIconToolTip;
  String iconSource;
  TextEditingController controller;
  Color prefixIconColor ;
  String textHint;
  CustomTextField({
    this.controller,
    this.errorIconToolTip ,
    this.prefixIconColor,
    this.textHint,
    this.iconSource
  });

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {

  static const Color inputTextColor = Color(0xffd2d3d1);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxWidth: 300),
      height: 45,
      child: new TextFormField(
        onChanged: (value) {
          if(value.isNotEmpty) {
            if(widget.errorIconToolTip!=null) {
              widget.errorIconToolTip = null;
              setState(() {

              });
            }
          }
        },
        controller: widget.controller,
        autocorrect: false,
        style: const TextStyle(
          color: Colors.black,
          fontFamily: "arial",
        ),
        decoration: new InputDecoration(
          suffixIcon: widget.errorIconToolTip,
          icon: Image.asset(widget.iconSource,
              color: widget.prefixIconColor,
              height: 20,
              width: 20),
          border: InputBorder.none,
          hintText: widget.textHint,
          hintStyle: const TextStyle(
              color: inputTextColor,
              fontFamily: "roboto-regular"),
        ),
      ),
    );
  }
}
