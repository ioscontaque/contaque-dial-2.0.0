import 'package:flutter/material.dart';

class CenteredWidget extends StatelessWidget {
  final Widget child;

  CenteredWidget({this.child});


  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        alignment: Alignment.center,
        constraints: BoxConstraints(
          maxWidth: 500
        ),
        child: child,
      ),
    );
  }
}
