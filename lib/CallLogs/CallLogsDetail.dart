import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dialer/HomePage/HomePage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dialer/Login/Login.dart';
import 'package:dialer/Url.dart';
import 'package:connectivity/connectivity.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:dialer/CallLogs/CallDetailModel.dart';
import 'convertDateToString.dart';
import 'convertTimeToString.dart';

class CallLogsDetail extends StatefulWidget {
  const CallLogsDetail({Key key}) : super(key: key);

  @override
  CallLogsDetailState createState() => new CallLogsDetailState();
}

class CallLogsDetailState extends State<CallLogsDetail> {
  List<CallDetailModel> arrHistoryData = [];
  Url url = Url();
  String urlValue;
  final txtAgent = new TextEditingController();
  static const platform = const MethodChannel('MyNativeChannel');
  List<String> dateChangedIndex = new List();
  ConvertTime convertTime = ConvertTime();
  ConvertDate convertDate = ConvertDate();
  SharedPreferences prefs;



  String ip;
  DateTime date;


  init() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    init();
    Timer(Duration(milliseconds: 500),(){
      serviceForGetData();
    });
    super.initState();
  }

  @override
  void dispose() {
    txtAgent.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: () async {
        Navigator.pop(context);
        return false;
      },
      child: Scaffold(
          backgroundColor: Color(0xffe9eaed),
          body: Stack(
            children: <Widget>[
              arrHistoryData!=null && arrHistoryData.length > 0 ?
          Container(
          margin: EdgeInsets.only(top: 80),
        child: Column(
          children: <Widget>[
            Container(
                margin: EdgeInsets.symmetric(horizontal: 6,vertical: 8),
                height: 60,
                color: Colors.white,
                child:Center(child: Text(arrHistoryData !=null&&arrHistoryData.length > 0 ? arrHistoryData[0].customerNumber : '' ?? '',style: TextStyle(color: Color(0xff0f0f0f)
                    , fontFamily: 'roboto-regular',fontSize: 20,fontWeight: FontWeight.w600
                ),),)),
            Expanded(
              child: Container(
                color: Colors.white,
                margin:EdgeInsets.symmetric(horizontal: 6,vertical: 2) ,
                child: ListView.builder(itemBuilder: (context,index){
                  return handledCallItem(index);
                },
                  itemCount: arrHistoryData.length,
                ),
              ),
            )
          ],
        ),
      )   :
              Center(
                  child: Container(
                      width: 30,
                      height: 30,
                      child: CircularProgressIndicator(
                      ))
              ),

              Container(
                height: 85,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    stops: [0.4, 0.8, 1],
                    colors: [
                      // Colors are easy thanks to Flutter's Colors class.
                      Color(0xff00476d),
                      Color(0xff007981),
                      Color(0xff00a969),
                    ],
                  ),
                ),
                child: Container(
                  margin: EdgeInsets.only(top: 12),
                  child: Row(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          Icons.arrow_back_ios,
                          size: 18,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                      Container(
                        child: Text(
                          "Details",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontFamily: 'roboto-regular'),
                        ),
                      )
                    ],
                  ),
                ),
              )

            ],
          ),
        )
    );
  }

  Widget handledCallItem(index) {
    if(!dateChangedIndex.contains(index.toString())) {
      return customCallItem(index);
    }
    else {
      return Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 12, right: 12),
            height: 30,
            width: MediaQuery.of(context).size.width,
            child: Container(
              margin: EdgeInsets.only(top: 0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                    child: Text(
                      convertDate.convert(arrHistoryData[index].date) ?? '#',
                      style: TextStyle(
                          color: Color(0xff828580),
                          fontWeight: FontWeight.bold,
                          fontFamily: 'roboto-regular',
                          fontSize: 14),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(left: 4),
                      height: 1,
                      color: Color(0xffa4a9aa),
                    ),
                  )
                ],
              ),
            ),
          ),
          customCallItem(index),
        ],
      );
    }
  }

  Widget customCallItem(index) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20,vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Text(arrHistoryData[index].agentName ?? '',style: TextStyle(color: Color(0xff0f0f0f)
                , fontFamily: 'roboto-regular',fontSize: 18
            ),),
          ),
          Container(
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(convertTime.convert(arrHistoryData[index].duration)?? '0 secs' , style: TextStyle(color: Color(0xff828580)
                    , fontFamily: 'roboto-regular',fontSize: 12
                )),
                Text(convertTime.convertToAMPM(arrHistoryData[index].date) ?? '0' , style: TextStyle(color: Color(0xff828580)
                    , fontFamily: 'roboto-regular',fontSize: 12
                )),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> serviceForGetData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = (prefs.getString('accessToken') ?? '');
    String iPAddress = prefs.getString("ipAddress") ?? '';
    String number = prefs.getString("customerNumber") ?? '';
    var connectivityResult = await (new Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      HttpOverrides.global = new MyHttpOverrides();
      var body = jsonEncode({
        "type": "APP-REQUEST",
        "data": {
          "logs_limit": 20,
          "customer_number":number
        }
      });

    try {
        await http.post(iPAddress + url.callLogs, body: body, headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "Authorization": token
        }).then((response) async {
          String responseBody = response.body;
          debugPrint("CallLOGSDETAILS: ${responseBody}");
          var responseJSON = json.decode(responseBody);
          if (response.statusCode == 200) {

            if (responseJSON["response"] == "SUCCESS") {
              var arrData = responseJSON["data"]["call_logs_data"];
              var list = arrData;
              if (list.isNotEmpty) {
                String lastDate = "!!";
                int index = 0;
                for (var itemsList in list) {
                  Map myMap = itemsList;
                  debugPrint("CALL DETAILS ${myMap}");
                  txtAgent.text = myMap["agent_name"];
                  if(lastDate != convertDate.convert(myMap["date"])) {
                    dateChangedIndex.add(index.toString());
                    lastDate = convertDate.convert(myMap["date"]);
                  }
                  arrHistoryData.add(CallDetailModel(
                      duration: myMap["duration"].toString() ??"",
                      date: myMap["date"].toString() ??"",
                      agentName: myMap["agent_name"] ?? "",
                      customerName: myMap["customer_name"]??"",
                      customerNumber: myMap["customer_number"].toString() ??""));
                }
              }
              setState(() {

              });
            }
            else if(responseJSON["response"] == "FAILURE"&&responseJSON["error"]["message"]=="User active from web"){
              Url.toastShow(responseJSON["error"]["message"]);
              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                  builder: (context)=>HomePage()
              ), (route) => false);
            }
            else {
              Url.toastShow(responseJSON["error"]["message"] ,  Colors.red);
              setState(() {

              });
            }
          }
          else if (responseJSON["response"] == "FAILURE" &&
              (responseJSON["error"]["message"] == "Authorization Failure")) {
            prefs.setBool('loginStatus', false);
            // prefs.remove("dialCode");
            prefs.remove("listid");
            prefs.remove("_allowCamp");
            prefs.remove("selectedData");
            Url.toastShow(responseJSON["error"]["message"], Colors.red);
            goToIntroPage();
          }
          else {
            Navigator.pop(context);
            Url.toastShow(responseJSON["error"]["message"] ,  Colors.red);
          }
        }).timeout(const Duration(seconds: 10));
      } on Exception catch (error) {
        Url.toastShow(
            "Something went wrong. try again later. \n Error Code : $error"
        , Colors.orange
        );
        Navigator.pop(context);
      }
    } else {
      Navigator.pop(context);
      Url.toastShow("Kindly check your internet connection!!");
    }
  }
  void goToIntroPage() {
    prefs.setBool("setCampDetails", false);
    prefs.setBool("loginStatus", false);
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => Login()),
            (Route<dynamic> route) => false);
  }
}
