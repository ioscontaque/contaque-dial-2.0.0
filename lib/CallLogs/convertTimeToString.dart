import 'package:intl/intl.dart';

class ConvertTime {
  int hours;
  int minute;
  int seconds;
  String convertedTime = "";


   convert(String time) {
     convertedTime = "";
    int localTime = int.parse(time);
    hours = localTime~/(3600) ;
    localTime = localTime - localTime~/(3600) * 3600 ;
    minute = localTime~/60 ;
    localTime = localTime - localTime~/60 * 60;
    seconds = localTime ;

    if(hours > 0) {
      convertedTime = hours.toString() + " hr " ;
    }
    convertedTime += minute.toString() + " mins ";
    convertedTime += seconds.toString() + " secs";

    return convertedTime;
  }

   convertToAMPM(String date) {
     DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
     DateTime dateTime = dateFormat.parse(date);
     convertedTime = "";
     bool isPM = false;
     hours = int.parse(dateTime.hour.toString());
     minute = int.parse(dateTime.minute.toString());
     String localMin ;
     if(minute <9){
       localMin = "0"+minute.toString();
     }
     else {
       localMin = minute.toString();
     }

     if(hours > 12) {
       hours = hours - 12 ;
       convertedTime = hours.toString() + ":" + localMin + " pm";
     }
     else {
       convertedTime = hours.toString() + ":" + localMin + " am";
     }

     return convertedTime ;
   }

}