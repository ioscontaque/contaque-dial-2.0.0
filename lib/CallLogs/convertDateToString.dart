import 'package:intl/intl.dart';

class ConvertDate{
  String convertedDate = "";
  List<String> MONTHS = ["January","February","March","April","May","June","July","August","September"
    ,"October","November","December"];

  convert(String date) {
    convertedDate = "";
    int currentDD =  DateTime.now().day;
    int currentMM = DateTime.now().month;
    int currentYY = DateTime.now().year;

    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    DateTime dateTime = dateFormat.parse(date);

    int dd = dateTime.day;
    int mm = dateTime.month;
    int yy = dateTime.year;

    if(dd == currentDD && mm == currentMM && yy == currentYY) {
      convertedDate = "TODAY";
    }
    else if(dd==currentDD+1 && mm == currentMM && yy == currentYY) {
      convertedDate = "YESTERDAY";
    }
    else {
      convertedDate = dd.toString() +" " + MONTHS[mm-1] + " "+ yy.toString();
    }

    return convertedDate.toUpperCase();

  }

}