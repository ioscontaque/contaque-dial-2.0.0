import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:dialer/CallLogs/convertDateToString.dart';
import 'package:dialer/HomePage/HomePage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:dialer/Model/CallDetailModel.dart';
import 'package:dialer/Url.dart';
import 'package:dialer/Login/Login.dart';
import 'package:http/http.dart' as http;
import 'package:dialer/CallLogs/CallLogsDetail.dart';

import '../custom_appbar.dart';
import 'convertTimeToString.dart';

class CallLogs extends StatefulWidget {
  const CallLogs({Key key}) : super(key: key);

  @override
  CallLogsState createState() => new CallLogsState();
}

class CallLogsState extends State<CallLogs> {
  List conferenceList;

  List<CallDetailModel> conference = [];
  Url url = new Url();
  bool loader = true;

  bool selection = false;
  List arrconfId = [];
  bool isEdit = true;
  SharedPreferences prefs ;
  ConvertTime convertTime = ConvertTime();
  ConvertDate convertDate = ConvertDate();
  String lastDate = "!!";
  List<String> dateChangedIndex = new List();

  @override
  void initState() {
    Timer(Duration(milliseconds: 500), (){
      fetchPosts();
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget _getList() {
    return conference!=null && conference.length > 0
     ? Container(
      margin: EdgeInsets.only(top: 80,bottom: 0),
      child: ListView.builder(
          itemCount:
          conference != null && conference.length > 0 ? conference.length+1 : 0,
          itemBuilder: (context, index) {
            convertTime.convertedTime = "";
            convertDate.convertedDate = "";
            return handledCallItem(index);
          }),
    ) :
    Center(
        child: Container(
            width: 30,
            height: 30,
            child: CircularProgressIndicator(
            ))
    )
    ;
  }

 Widget handledCallItem(index){
    if(!dateChangedIndex.contains(index.toString())) {
      return customCallItem(index);
    }
    else {
      return Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 12, right: 12),
            height: 30,
            width: MediaQuery.of(context).size.width,
            child: Container(
              margin: EdgeInsets.only(top: 10),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                    child: Text(
                      convertDate.convert(conference[index].date) ?? '#',
                      style: TextStyle(
                          color: Color(0xff828580),
                          fontWeight: FontWeight.bold,
                          fontFamily: 'roboto-regular',
                          fontSize: 14),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(left: 4),
                      height: 1,
                      color: Color(0xffa4a9aa),
                    ),
                  )
                ],
              ),
            ),
          ),
          customCallItem(index),
        ],
      );
    }
 }

 Widget customCallItem(index) {
    if(index == conference.length) {
      return SizedBox(
        height: 20,
      );
    }
   return GestureDetector(
     onTap: (){
       prefs.setString("customerNumber",conference[index].phoneNumber ??'') ;
       Navigator.push(context, MaterialPageRoute(builder: (context)=>CallLogsDetail()));
     },
     child: Container(
       padding: EdgeInsets.only(left: 30,right:10,top: 8),
       child: Column(
           crossAxisAlignment: CrossAxisAlignment.start,
           mainAxisAlignment: MainAxisAlignment.start,
           children: <Widget>[
             Container(
               width: MediaQuery.of(context).size.width,
               child: Text(conference[index].phoneNumber ?? '',style: TextStyle(color: Color(0xff0f0f0f)
                   , fontFamily: 'roboto-regular',fontSize: 18,
               ),),
             ),
             Container(
               child:Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: <Widget>[
                   Text(convertTime.convert(conference[index].duration)  ?? '0' , style: TextStyle(color: Color(0xff828580)
                       , fontFamily: 'roboto-regular',fontSize: 12
                   )),
                   Text(convertTime.convertToAMPM(conference[index].date) ?? '00:00 am' , style: TextStyle(color: Color(0xff828580)
                       , fontFamily: 'roboto-regular',fontSize: 12
                   )),
                 ],
               ),
             ),
           ],
         ),

     ),
   );
 }


  @override
  Widget build(BuildContext context) {
    return  Scaffold(
          backgroundColor: Colors.white,
          body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: <Widget>[
                _getList(),
                CustomAppBar(title: "Call logs",),
              ],
            ),
          )
          //_getList(),
        )
    ;
  }

  Future<void>fetchPosts() async {
    conference = [];
    conferenceList = [];
    prefs = await SharedPreferences.getInstance();
    var token = (prefs.getString('accessToken') ?? '');
    String iPAddress = prefs.getString("ipAddress") ?? '';
    var connectivityResult = await (new Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      HttpOverrides.global = new MyHttpOverrides();
      var body = jsonEncode({
            "type": "APP-REQUEST",
            "data": {
              "logs_limit": 100
            }});
      debugPrint("CALLLOGS ${iPAddress+url.callLogs} , body : $body , accessT $token");

    try {
        await http.post(iPAddress + url.callLogs, body: body, headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "Authorization": token
        }).then((response) async {
          String responseBody = response.body;
          debugPrint("CallLOGS: ${responseBody}");
          var responseJSON = json.decode(responseBody);
          if (response.statusCode == 200) {

          if (responseJSON["response"] == "SUCCESS") {
              var arrData = responseJSON["data"]["call_logs_data"];
              var list = arrData;
              if (list.isNotEmpty) {
                String lastDate = "!!";
                int i = 0;
                for (var itemsList in list) {
                  Map myMap = itemsList;
                  if(lastDate != convertDate.convert(myMap['date'])) {
                    dateChangedIndex.add(i.toString());
                    lastDate = convertDate.convert(myMap["date"]);
                  }
                  i++;
                  conference.add(CallDetailModel(
                    phoneNumber: myMap['customer_number'],
                    duration: myMap['duration'].toString(),
                    date: myMap['date'],
                  ));
                }
              }
              setState(() {
              });
            }
          else if(responseJSON["response"] == "FAILURE"&&responseJSON["error"]["message"]=="User active from web"){
            Url.toastShow(responseJSON["error"]["message"]);
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                builder: (context)=>HomePage()
            ), (route) => false);
          }
          else {
              Url.toastShow(responseJSON["error"]["message"], Colors.red);
              setState(() {
              });
            }
          }
          else if (responseJSON["response"] == "FAILURE" &&
              (responseJSON["error"]["message"] == "Authorization Failure")) {
            prefs.setBool('loginStatus', false);
            // prefs.remove("dialCode");
            prefs.remove("listid");
            prefs.remove("_allowCamp");
            prefs.remove("selectedData");
            Url.toastShow(responseJSON["error"]["message"], Colors.red);
            goToIntroPage();
          }
          else

            {
            Url.toastShow(responseJSON["error"]["message"], Colors.red);
            setState(() {
            });
          }
        }).timeout(const Duration(seconds: 10));
      } on Exception catch (error) {
        Url.toastShow(
            "Something went wrong. try again later. \n Error : $error",
            Colors.orange);
        setState(() {
        });
      }
    } else {
      Url.toastShow("Kindly check your internet connection!!");
    }
  }

  void goToIntroPage() {
    prefs.setBool("setCampDetails", false);
    prefs.setBool("loginStatus", false);
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => Login()),
            (Route<dynamic> route) => false);
  }

  void _onLoading() {
    if (loader == true) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => new Center(
          child: CircularProgressIndicator(
            valueColor:
                new AlwaysStoppedAnimation<Color>(new Color(0xFF4B8BF4)),
          ),
        ),
      );
    } else {
      Navigator.pop(context);
    }
  }
}
