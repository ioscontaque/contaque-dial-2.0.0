class CallDetailModel {
  final String duration,
      date,
      agentName,
      customerNumber,
      customerName;

  CallDetailModel({
    this.duration,
    this.date,
    this.agentName,
    this.customerNumber,
    this.customerName,

  });
}
