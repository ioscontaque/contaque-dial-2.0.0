package com.go4call.dialer;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import androidx.annotation.NonNull;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "MyNativeChannel";
    String text;
    String getStatus = "free";

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
                .setMethodCallHandler(
                        (call, result) -> {
                            try{
                                if (call.method.equals("getData")) {
                                    text = call.argument("text");

                                    getData();
                                } else  if(call.method.equals("getCallStatus")){
                                    callState();
                                    String greetings = gatCallStatus();
                                    result.success(greetings);
                                }
                                else{
                                    result.notImplemented();
                                }
                            } catch (Exception e) {

                            }
                        }
                );
    }

    private void getData() {
        String number=text;
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+number));
        startActivity(callIntent);
    }

    private String gatCallStatus() {

        return getStatus;
    }


    private  void callState() {
        TelephonyManager telephonyManager =
                (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);

        PhoneStateListener callStateListener = new PhoneStateListener() {

            public void onCallStateChanged(int state, String incomingNumber)
            {

                if(state==TelephonyManager.CALL_STATE_RINGING){

                    getStatus = "PhoneIsRiging";


                }
                if(state==TelephonyManager.CALL_STATE_OFFHOOK){

                    getStatus = "CallConnect";

                }

                if(state==TelephonyManager.CALL_STATE_IDLE){

                    getStatus = "free";

                }


            }
        };


        telephonyManager.listen(callStateListener,PhoneStateListener.LISTEN_CALL_STATE);

    }

}
