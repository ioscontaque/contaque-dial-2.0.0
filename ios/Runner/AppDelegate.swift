import UIKit
import Flutter
import CallKit

@available(iOS 10.0, *)
@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate , CXCallObserverDelegate {
    var callObserver = CXCallObserver()
    var text : String = "" ;
    var callStatus = "";
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        print("Get Call");

        callObserver.setDelegate(self, queue: nil)
        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
        let channel = FlutterMethodChannel(name: "MyNativeChannel",
                                           binaryMessenger: controller.binaryMessenger)
                                                  print(channel);
        channel.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: FlutterResult) -> Void in
            
            switch(call.method)
            {
                case "getCallStatus":
                    let status = self?.getCallStatus();
                    print("status",status as Any)
                    result(status)
                    break;
                case "getData":
                    let number : NSDictionary = call.arguments as? NSDictionary ?? [:]
                    print("number",number)
                    self?.text = number.object(forKey: "text") as! String;
                    self?.getData();
                    break;
               default:
                    result(FlutterMethodNotImplemented);
                    break;
            }
        })
        GeneratedPluginRegistrant.register(with: self)
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    private func getData() {
        if let phoneCallURL = URL(string: "telprompt://\(text)") {

            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)

                }
            }
        }
    }

     private func getCallStatus() -> String{
        return callStatus;
    }
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {

        if call.hasConnected {
            callStatus = "Call Connect"
            print("Call Connect -> \(call.uuid)")
        }

        if call.isOutgoing {
              callStatus = "Call outGoing"
            print("Call outGoing \(call.uuid)")
        }

        if call.hasEnded {
             callStatus = "free"
            print("Call hasEnded \(call.uuid)")
        }

        if call.isOnHold {
              callStatus = "Call onHold"
            print("Call onHold \(call.uuid)")
        }

    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
